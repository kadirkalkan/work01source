﻿; (function ($) {
    $.fn.datepicker.language['tr'] = {
        days: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        daysShort: ['Paz', 'Pzt', 'Sal', 'Çar', 'Per', 'Cum', 'Cmt'],
        daysMin: ['P', 'Pzt', 'S', 'Ç', 'P', 'C', 'Cts'],
        months: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        monthsShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
        today: 'Bugün',
        clear: 'Temizle',
        dateFormat: 'mm/dd/yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    };
})(jQuery);
// Orjinal AirDatePicker js dosyasını update ettim. Bu dosyaya gerek kalmadı.