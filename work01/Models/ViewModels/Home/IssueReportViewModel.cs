﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using work01.Models.Entities;

namespace work01.Models.ViewModels.Home
{
    public class IssueReportViewModel
    {
        public User Employee { get; set; }
        public User Customer { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }

        public Status Status { get; set; }


        public int issueCount { get; set; }
        public int openIssueCount { get; set; }
        public int closeIssueCount { get; set; }
        public int waitingIssueCount { get; set; }
        public int criticalIssueCount { get; set; }
        public int importantIssueCount { get; set; }
        public int normalIssueCount { get; set; }
        public string totalSupportTime { get; set; }

            

    }
}