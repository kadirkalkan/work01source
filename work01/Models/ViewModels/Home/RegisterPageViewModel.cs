﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using work01.Models.Entities;
namespace work01.Models.ViewModels.Home
{
    public class RegisterPageViewModel
    {
        public Entities.User User { get; set; }

        public Person Person { get; set; }

        public Customer Customer { get; set; }

        public string contractEndTime { get; set; }

    }
}