﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace work01.Models.ViewModels.Home
{
    public class IndexViewModel
    {
        public int issueCount { get; set; }
        public int issueOnUserCount { get; set; }
        public int openedIssuesByUser { get; set; }
        public int openIssueOfAppointedUser { get; set; }
        public int openIssueCount { get; set; }
        public int doneIssueCount { get; set; }
    }
}