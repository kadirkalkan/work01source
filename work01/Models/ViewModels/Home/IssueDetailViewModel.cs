﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace work01.Models.ViewModels.Home
{
    public class IssueDetailViewModel
    {
        public int IssueID { get; set; }

        public int UserID { get; set; }

        public string UserName { get; set; }

        public string UserNameSurname { get; set; }

        public string IssueText { get; set; }

        public string issueTitle { get; set; }

        public int AppointedUserID { get; set; }

        public bool DemandApproval { get; set; }

        public string AppointedUser { get; set; }

        public int issueDetailID { get; set; }
        [Required]
        public string Message { get; set; }

        public DateTime? CreatedTime { get; set;}
    }
}