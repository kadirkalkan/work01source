﻿using System;
using System.Linq;
using System.Web;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using System.Collections.Generic;
using static work01.Models.Enums.EnumClass;
using System.Text;
using work01.Content;
using System.Resources;
using work01.Models.Enums;

namespace work01.Models.Managers
{
    public class Sendgrid
    {

        External external = new External();
        public void SendEMail(EmailAddress to, MailType mailType = MailType.reminder,string title = null, string text = null, List<string> mailContent = null, string btnURL = "")
        {
            try
            {
                string subject = "";
                mailContent = mailContent == null || mailContent.Count == 0 ? new List<string>() : mailContent;
                switch (mailType)
                {
                    case MailType.welcome: subject = "Hoşgeldiniz"; break;
                    case MailType.congratulation: subject = title == null ? "Tebrikler" : title; break;
                    case MailType.reminder: subject = "Sistem Hatırlatıcısı"; break;
                    case MailType.forgotPass: subject = "Şifrenizi Yenileyin"; break;
                }

                var apiKey = "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";
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("work01@freelance.com", "Kadir Kalkan");
                var plainTextContent = "All rights reserved";
                var htmlContent = CreateHtmlContent(mailType, title, text, mailContent, buttonURL: btnURL); ;
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                external.addLog(LogType.error, ex.ToString(), "Sendgrid", "SendEMail");
            }
        }


        public string CreateHtmlContent(MailType mailType, string title=null, string text=null, List<string> mailContent = null, string buttonURL = "")
        {
            string returnValue = "";
            try
            {
                switch (mailType)
                {
                    case MailType.welcome: returnValue = createCongratulationPage("Aramıza Hoşgeldiniz", "Kayıt olduğunuz için teşekkür ederiz. Herhangi bir sorununuz olduğunda sistemimiz üzerinden yardım talep edebilirsiniz."); break;
                    case MailType.congratulation: returnValue = createCongratulationPage(title, text); break;
                    case MailType.reminder: returnValue = createReminderPage(mailContent: mailContent); break;
                    case MailType.forgotPass: returnValue = createRedirectPage("Şifrenizi Yenileyin", "Merhaba "+text+ ";</p><p style='font-weight:normal; color:#677483;font-family:sans-serif;font-size:14px;'>Aşağıdaki butona tıklayarak gideceğiniz sayfadan şifrenizi yenileyebilirsiniz", buttonURL, "Yeni Şifre Al"); break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                external.addLog(LogType.error, ex.ToString(), "Sendgrid", "CreateHtmlContent");
            }
            return returnValue;
        }

        private string createCongratulationPage(string title = null, string text = null)
        {
            title = String.IsNullOrEmpty(title) ? "Hatırlatıcı" : title;
            text = String.IsNullOrEmpty(title) ? "Hesabınızda Aktiflik Gözlemlendi." : text;

            return getFullTemplate(title, text, addFooter: true, footerHeader: "Teşekkürler", footerText: "Kadir Kalkan ve work01 Ekibi ");
        }
        private string createReminderPage(string title=null ,List<string> mailContent = null)
        {
            title = String.IsNullOrEmpty(title) ? "Hatırlatıcı Notlar" : title;
            string mailList = "Görüntülenecek Yeni Haber Yok.";
            if (mailContent != null && mailContent.Count > 0)
            {
                mailList = "<ul><li>" + String.Join("</li><li>", mailContent) + "</li></ul>";
            }
            return getFullTemplate(title + " : ", mailList, addFooter: true, footerHeader: "Teşekkürler", footerText: "Kadir Kalkan ve work01 Ekibi ");
        }
        private string createRedirectPage(string title=null, string text=null, string btnURL = null, string buttonText=null)
        {
            title = String.IsNullOrEmpty(title) ? "Yönlendirici Sistemi" : title;
            text = String.IsNullOrEmpty(text) ? "İlgili Sayfaya Yönlendirilmek İçin Aşağıdaki Buton'u Tıklayınız." : text;
            btnURL = String.IsNullOrEmpty(btnURL) ? "#" : btnURL;
            buttonText = String.IsNullOrEmpty(buttonText) ? "Yönlendir" : buttonText;
            return getFullTemplate(title, text, addButton: true, buttonURL: btnURL, buttonText: buttonText, addFooter: true, footerHeader: "Teşekkürler", footerText: "Kadir Kalkan ve work01 Ekibi ");
        }

        private string getFullTemplate(string header, string innerText, bool addButton = false, string buttonURL = "#", string buttonText = "", bool addFooter = false, string footerHeader = "", string footerText = "")
        {
            string returnValue = "";
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Resources.templateStart);

                stringBuilder.Append(Resources.templateContentHeaderStart);
                stringBuilder.Append("<h1 style='font-family:sans-serif;font-weight:bold;Margin-top:0;text-align:left;font-size:22px;line-height:26px;Margin-bottom:20px;color:#445359;'>" + header + "</h1>");
                stringBuilder.Append("<p style = 'Margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;Margin-bottom:15px;' > " + innerText + "</p >");
                stringBuilder.Append(Resources.templateContentHeaderEnd);

                if (addButton)
                {
                    stringBuilder.Append(Resources.templateContentButtonStart);
                    stringBuilder.Append("<a href='" + buttonURL + "' style='mso-hide:all;background-color:#62a30c;background-image:url(https://img.createsend1.com/img/onboarding/btn-green.png);background-repeat:repeat-x;border-width:1px;border-style:solid;border-color:#538c02;border-radius:3px;color:#ffffff;display:inline-block;font-weight:normal;font-family:sans-serif;font-size:14px;line-height:45px;text-align:center;text-decoration:none;-webkit-text-size-adjust:none;text-shadow:1px 2px 1px #548e19;box-shadow:0px 2px 2px 0px #e2ecd4;-webkit-box-shadow:0px 2px 2px 0px #e2ecd4;-moz-box-shadow:0px 2px 2px 0px #e2ecd4;width:260px;'>" + buttonText + "</a>");
                    stringBuilder.Append(Resources.templateContentButtonEnd);
                }
                if (addFooter)
                {
                    stringBuilder.Append(Resources.templateContentFooterStart);
                    stringBuilder.Append("  <p style='Margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;Margin-bottom:15px;'>");
                    stringBuilder.Append(footerHeader);
                    stringBuilder.Append("  <br />");
                    stringBuilder.Append("  <p style='Margin-top:0;font-weight:normal;color:#677483;font-family:sans-serif;font-size:14px;line-height:25px;Margin-bottom:15px;'>");
                    stringBuilder.Append("  <em style = 'color:#a9b7c8;font-style:italic;' > " + footerText + " </em >");
                    stringBuilder.Append(" </p >");
                    stringBuilder.Append(Resources.templateContentFooterEnd);
                }
                stringBuilder.AppendLine(Resources.templateEnd);


                returnValue = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                external.addLog(LogType.error, ex.ToString(), "Sendgrid", "getFullTemplate");
            }
            return returnValue;
        }
    }
}