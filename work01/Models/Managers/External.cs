﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using work01.Models.Entities;

namespace work01.Models.Managers
{
    public class External
    {
        DatabaseContext context = new DatabaseContext();
        private static string baseURL = "http://localhost/";
        public string GenerateSha256(string yourString)
        {
            string returnValue = "";
            try
            {
                returnValue = yourString != null ? string.Join("", new SHA256Managed().ComputeHash(Encoding.ASCII.GetBytes(yourString))) : "";
            }
            catch (Exception ex)
            {
                addLog(Enums.EnumClass.LogType.error, ex.ToString(), "External", "GenerateSha256");
            }
            return returnValue;
        }

        public bool IsAccessible(string pageName, List<WebPageClass> forbiddenPageList)
        {
            bool redirectToIndex = true;
            foreach (WebPageClass page in forbiddenPageList)
            {
                if (page.PageName.Equals(pageName))
                {
                    redirectToIndex = false;
                    break;
                }
            }
            return redirectToIndex;
        }

        public bool addLog(Enums.EnumClass.LogType logTypeValue = Enums.EnumClass.LogType.info, string logText = "", string className = "-", string methodName = "-")
        {
            bool returnValue = false;
            className = String.IsNullOrEmpty(className) ? "-" : className;
            methodName = String.IsNullOrEmpty(methodName) ? "-" : methodName;
            try
            {
                string logTypeText = logTypeValue.ToString();
                LogType logType = context.LogTypes.FirstOrDefault(x => x.LogTypeText == logTypeText);
                if (logType != null && logText != null && logText.Length > 0)
                {
                    Log log = new Log();
                    log.LogType = logType;
                    log.LogText = logText;
                    log.ClassName = className;
                    log.MethodName = methodName;
                    log.CreatedTime = DateTime.Now;
                    context.Logs.Add(log);
                    returnValue = context.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public string getPageURL(Enums.EnumClass.Page input = Enums.EnumClass.Page.login)
        {
            string url = baseURL;
            switch (input)
            {
                case Enums.EnumClass.Page.login:
                    url += "User/LoginPage";
                    break;
                case Enums.EnumClass.Page.forgotPass:
                    url += "User/ForgotPasswordPage";
                    break;
                case Enums.EnumClass.Page.repass:
                    url += "User/RePasswordPage";
                    break;
                case Enums.EnumClass.Page.dashboard:
                    url += "Home/Index";
                    break;
                case Enums.EnumClass.Page.profile:
                    url += "Home/ProfilePage";
                    break;
                case Enums.EnumClass.Page.issues:
                    url += "Home/Issues";
                    break;
                case Enums.EnumClass.Page.users:
                    url += "Home/Users";
                    break;
                default:
                    url = "#";
                    addLog(Enums.EnumClass.LogType.error, "getPageURL()'e tanımlanmayan tipte kayıt geldi.", "External", "getPageURL");
                    break;
            }
            return url;
        }

        public DatabaseContext RefreshAll(DatabaseContext databaseContext)
        {
            if (databaseContext != null)
                databaseContext.Database.Connection.Close();
            return new DatabaseContext();
        }

        internal string GetFormattedTimeString(DateTime time, string type = "MM / dd / yyyy HH:mm")
        {
            type = String.IsNullOrEmpty(type) ? "MM / dd / yyyy HH:mm" : type;
            return time.ToString(type);
        }
    }


}