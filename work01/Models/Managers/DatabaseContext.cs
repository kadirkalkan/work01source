﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using work01.Models.Entities;
using work01.Models.Enums;

namespace work01.Models.Managers
{
    public class DatabaseContext : DbContext
    {
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<IssueDetail> IssueDetails { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<WebPageClass> Pages { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<LogType> LogTypes { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<RequestType> RequestTypes { get; set; }

        public DatabaseContext()
        {
            Database.SetInitializer(new DatabaseCreator());
        }


        public class DatabaseCreator : CreateDatabaseIfNotExists<DatabaseContext>
        {
            protected override void Seed(DatabaseContext context)
            {
                string[] statusValues = new string[] { "Beklemede", "Aktif", "Tamamlandı" };
                string[] contractTypeValues = new string[] { "ContractType1", "ContractType2", "ContractType3" };
                string[] userTypeValues = new string[] { "Administrator", "Employee", "Customer" };
                string[] pageValues = new string[] { "dashboard", "profile", "issues", "users", "logs" };
                string[] commonPermissions = new string[] { "dashboard", "profile", "issues" };
                string[] logTypeValues = new string[] { "info", "error" };
                string[] requestTypeValues = new string[] { "get", "cancel" };



                // Defaul REQUEST_TYPE Kayıtlarının Oluşturulması
                foreach (string requestTypeValue in requestTypeValues)
                {
                    RequestType newRequestType = new RequestType();
                    newRequestType.RequestTypeText = requestTypeValue;
                    context.RequestTypes.Add(newRequestType);
                }

                // Defaul LOG_TYPE Kayıtlarının Oluşturulması
                foreach (string logTypeValue in logTypeValues)
                {
                    LogType newLogType = new LogType();
                    newLogType.LogTypeText = logTypeValue;
                    context.LogTypes.Add(newLogType);
                }

                // Defaul STATUS Kayıtlarının Oluşturulması
                foreach (string statusValue in statusValues)
                {
                    Status newStatus = new Status();
                    newStatus.StatusType = statusValue;
                    context.Statuses.Add(newStatus);
                }

                // Defaul CONTRACT_TYPE Kayıtlarının Oluşturulması
                foreach (string contractTypeValue in contractTypeValues)
                {
                    ContractType newContractType = new ContractType();
                    newContractType.ContractTypeText = contractTypeValue;
                    context.ContractTypes.Add(newContractType);
                }

                // Defaul USER_TYPE Kayıtlarının Oluşturulması
                foreach (string userTypeValue in userTypeValues)
                {
                    UserType newUserType = new UserType();
                    newUserType.UserTypeText = userTypeValue;
                    context.UserTypes.Add(newUserType);
                }

                // Defaul PAGE Kayıtlarının Oluşturulması
                foreach (string page in pageValues)
                {
                    WebPageClass newPage = new WebPageClass();
                    newPage.PageName = page;
                    context.Pages.Add(newPage);
                }

                // Defaul PERSON Kayıdının Oluşturulması
                Person person = new Person();
                person.Name = "Kadir";
                person.SurName = "Kalkan";
                person.Phone = "5550348088";
                person.Adres = "Istanbul";
                context.People.Add(person);

                // Saving for ID creation.
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException dbValEx)
                {
                    handleError(dbValEx);
                }

                // Defaul PERMISSION Kayıtlarının Oluşturulması
                List<WebPageClass> pages = context.Pages.ToList();
                List<UserType> userTypes = context.UserTypes.ToList();

                foreach (WebPageClass p in pages)
                {
                    foreach (UserType uT in userTypes)
                    {
                        Permission newPermision = new Permission();
                        if (!uT.UserTypeText.Equals(EnumClass.UserType.Administrator.ToString()))
                        {
                            if (commonPermissions.Contains(p.PageName))
                            {
                                newPermision.pass = true;
                            }
                            else {
                                newPermision.pass = false;
                            }
                        }
                        newPermision.Page = p;
                        newPermision.UserType = uT;
                        context.Permissions.Add(newPermision);
                    }
                }


                // Defaul Administrator USER Kayıdının Oluşturulması
                person = context.People.ToList()[0];
                User user = new User();
                UserType userType = context.UserTypes.FirstOrDefault(x => x.UserTypeText.Equals("Administrator"));
                user.Person = person;
                user.Username = "admin";
                user.Password = new External().GenerateSha256("root");
                user.PasswordAgain = user.Password;
                user.Email = "kadir.kalkan3475@gmail.com";
                user.UserType = userType;
                user.GUID = Guid.NewGuid();

                context.Users.Add(user);

                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException dbValEx)
                {
                    handleError(dbValEx);
                }

            }

            protected void handleError(DbEntityValidationException dbValEx)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in dbValEx.EntityValidationErrors)
                {
                    outputLines.AppendFormat("{0}: Entity of type { 1} in state { 2} has the following validation errors:", DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.AppendFormat("- Property: { 0}, Error: { 1}", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //Tools.Notify(this, outputLines.ToString(),"error");
                throw new DbEntityValidationException(string.Format("Validation errorsrn{0}", outputLines.ToString()), dbValEx);
            }
        }
    }
}