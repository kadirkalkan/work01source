﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace work01.Models.Enums
{
    public class EnumClass
    {
        public enum ProcessType {
            initialize,
            @new,
            info, 
            danger,
            success,
            warning,
            rose,
            primary
        }
        public enum MailType
        {
            welcome,
            congratulation,
            forgotPass,
            reminder
        }

        public enum LogType
        {
            info,
            error
        }

        public enum RequestType
        {
            get,
            cancel
        }

        public enum Page
        {
            login,
            forgotPass,
            repass,
            dashboard,
            profile,
            issues,
            users,
            logs,
            openIssuesReport,
            closeIssuesReport,
            companyScoreReport,
            employeeReport
        }

        public enum StatusType
        {
            Beklemede, 
            Aktif, 
            Tamamlandı
        }

        public enum UserType {
            [Display(Name = "Administrator")]
            Administrator = 0,
            [Display(Name = "Çalışan")]
            Employee = 1,
            [Display(Name = "Müşteri")]
            Customer = 2
        }

    }
}