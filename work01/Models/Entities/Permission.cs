﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("PERMISSION")]
    public class Permission
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public virtual UserType UserType { get; set; }

        [Required]
        public virtual WebPageClass Page { get; set; }

        [DisplayName("Yetki"), Required]
        public bool pass { get; set; }

        public Permission() {
            pass = true;
        }
    }
}