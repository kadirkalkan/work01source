﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("CUSTOMER")]
    public class Customer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public virtual Person Person { get; set; }

        [DisplayName("Müşteri Kodu")]
        public string CustomerCode { get; set; }

        [DisplayName("Yetkili Telefon Numarası"), StringLength(20), DataType(DataType.PhoneNumber)]
        public string ManagerPhone { get; set; }


        [Required]
        public virtual ContractType ContractType { get; set; }

        [Required]
        public DateTime ContractEndTime { get; set; }
    }
}