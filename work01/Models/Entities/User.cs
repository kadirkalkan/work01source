﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("USER")]
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DisplayName("Kulanıcı Adı"),StringLength(50), Required, Index(IsUnique = true)]
        public string Username { get; set; }

        [DisplayName("Şifre"), Required]
        public string Password { get; set; }

        private string PasswordAgainParam = null;

        [DisplayName("Şifre (Tekrar)"), NotMapped, Compare(nameof(Password))]
        public string PasswordAgain
        {
            get
            {
                return String.IsNullOrEmpty(this.PasswordAgainParam) ?  Password : this.PasswordAgainParam;
            }
            set { this.PasswordAgainParam = value; }
        }

        [DisplayName("E-Mail"), StringLength(255), Required, Index(IsUnique = true), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public virtual Person Person { get; set; }

        [Required]
        public virtual UserType UserType { get; set; }

        private DateTime? createdTime = DateTime.Now;

        public DateTime CreatedTime
        {
            get { return this.createdTime.HasValue ? this.createdTime.Value : DateTime.Now; }
            set { this.createdTime = value; }
        }

        public Guid GUID { get; set; }
    }
}