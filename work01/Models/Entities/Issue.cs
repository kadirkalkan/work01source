﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("ISSUE")]
    public class Issue
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DisplayName("Talep Başlığı"), Required]
        public string IssueTitle { get; set; }

        [DisplayName("Talep İçeriği"), Required]
        public string IssueText { get; set; }

        [Range(0,2, ErrorMessage = "Önem Derecesi Seçmek Zorunludur ")]
        public int Priority { get; set; }

        [Required]
        public virtual Status Status { get; set; }

        public virtual RequestType RequestType { get; set; }

        public bool DemandApproval { get; set; }

        public virtual User AppointedUser { get; set; }

        public virtual User OpenedBy { get; set; }

        private DateTime? createdTime = DateTime.Now;

        public DateTime CreatedTime
        {
            get { return this.createdTime.HasValue ? this.createdTime.Value : DateTime.Now; }
            set { this.createdTime = value; }
        }

        public DateTime? AppointedTime { get; set; }
        public virtual User ClosedBy { get; set; }
        public DateTime? ClosedTime { get; set; }


    }
}