﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("ISSUE_DETAIL")]
    public class IssueDetail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public virtual Issue Issue { get; set; }

        [DisplayName("Mesaj"), Required]
        public string Message { get; set; }

        public string FileName { get; set; }

        [Required]
        public virtual User User { get; set; }


        private DateTime? createdTime = null;
        [Required]
        public DateTime CreatedTime {
            get { return this.createdTime.HasValue ? this.createdTime.Value : DateTime.Now; }
            set { this.createdTime = value; }
        }
    }
}