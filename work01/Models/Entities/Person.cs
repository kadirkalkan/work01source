﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("PERSON")]
    public class Person
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DisplayName("Ad"), StringLength(50), Required]
        public string Name { get; set; }

        [DisplayName("Soyad"), StringLength(50), Required]
        public string SurName { get; set; }

        [DisplayName("Telefon"), StringLength(20), DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [StringLength(255)]
        public string Adres { get; set; }

    }
}