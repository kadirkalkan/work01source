﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("REQUEST_TYPE")]
    public class RequestType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DisplayName("İstek Tipi")]
        public string RequestTypeText { get; set; }
    }
}