﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace work01.Models.Entities
{
    [Table("CONTRACT_TYPE")]
    public class ContractType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DisplayName("Kontrat Tipi"), StringLength(50), Required]
        public string ContractTypeText { get; set; }
    }
}