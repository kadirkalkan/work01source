﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace work01
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            #region UserController 
            routes.MapRoute(
                          name: "Default",
                          url: "{controller}/{action}/{id}",
                          defaults: new { controller = "User", action = "LoginPage", id = UrlParameter.Optional }
                      );

            routes.MapRoute(
                           name: "Login",
                           url: "User/LoginPage/{id}",
                           defaults: new { controller = "User", action = "LoginPage", id = UrlParameter.Optional }
                       );

            routes.MapRoute(
                name: "ForgotPasswordPage",
                url: "User/ForgotPasswordPage/{id}",
                defaults: new { controller = "User", action = "ForgotPasswordPage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "RePasswordPage",
                url: "User/RePasswordPage/{id}",
                defaults: new { controller = "User", action = "RePasswordPage", id = UrlParameter.Optional }
            );
            #endregion

            #region HomeController 
            routes.MapRoute(
                name: "TemplatePage",
                url: "Home/TemplatePage/{id}",
                defaults: new { controller = "Home", action = "TemplatePage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Index",
                url: "Home/Index/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ProfilePage",
                url: "Home/ProfilePage/{id}",
                defaults: new { controller = "Home", action = "ProfilePage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Issues",
                url: "Home/Issues/{id}",
                defaults: new { controller = "Home", action = "Issues", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "IssueDetail",
                url: "Home/IssueDetail/{id}",
                defaults: new { controller = "Home", action = "IssueDetail", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "DeleteFileForIssue",
              url: "Home/DeleteFileForIssue/{id}",
              defaults: new { controller = "Home", action = "DeleteFileForIssue", id = UrlParameter.Optional }
          );

            routes.MapRoute(
                name: "Users",
                url: "Home/Users/{id}",
                defaults: new { controller = "Home", action = "Users", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "RegisterPage",
                url: "Home/RegisterPage/{id}",
                defaults: new { controller = "Home", action = "RegisterPage", id = UrlParameter.Optional }
            );
            #endregion
        }
    }
}
