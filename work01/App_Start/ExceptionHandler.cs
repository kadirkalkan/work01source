﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using work01.Models.Enums;
using work01.Models.Managers;

namespace work01.App_Start
{
    public class ExceptionHandler : FilterAttribute, IExceptionFilter
    {
        External external = new External();
        void IExceptionFilter.OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            //Log the error!!
            external.addLog(EnumClass.LogType.error, filterContext.Exception.ToString(), filterContext.RouteData.Values["controller"].ToString() + "Controller", filterContext.RouteData.Values["action"].ToString());
            // RouteToLogin
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "LoginPage", controller = "User", @enum = Models.Enums.EnumClass.ProcessType.danger, processMessage = "Bir hata meydana geldi lütfen yetkiliye bildirin." }));
        }
    }
}