﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using work01.App_Start;
using work01.Content;
using work01.Filters;
using work01.Models.Entities;
using work01.Models.Enums;
using work01.Models.Managers;
using work01.Models.ViewModels.Home;

namespace work01.Controllers
{
    [ExceptionHandler]
    public class HomeController : Controller
    {
        DatabaseContext context = new DatabaseContext();
        External external = new External();
        #region Template
        public ActionResult TemplatePage()
        {
            if (reloadViewBagFromSession("Gösterge Paneli", EnumClass.Page.dashboard))
            {
                return View();
            }
            else
            {
                return returnToLogin();
            };
        }
        #endregion

        #region Index
        [AuthFilter(PageName = "dashboard")]
        public ActionResult Index(EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "")
        {
            if (reloadViewBagFromSession("Gözlem Paneli", EnumClass.Page.dashboard))
            {
                setProcess(@enum, processMessage);
                IndexViewModel index = new IndexViewModel();
                int logged_user_id = ViewBag.logged_user.id;
                string userType = ViewBag.logged_user.userType;
                ViewBag.userType = userType;
                context = external.RefreshAll(context);
                index.issueCount = context.Issues.Count();
                if (userType.Equals(EnumClass.UserType.Customer.ToString()))
                {
                    index.openedIssuesByUser = context.Issues.Count(x => x.OpenedBy.ID == logged_user_id);
                }
                else
                {
                    index.issueOnUserCount = context.Issues.Count(x => x.AppointedUser.ID == logged_user_id);
                    index.openedIssuesByUser = context.Issues.Count(x => x.OpenedBy.ID == logged_user_id);
                    index.openIssueOfAppointedUser = context.Issues.Count(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()) && x.AppointedUser.ID == logged_user_id);
                }
                index.openIssueCount = context.Issues.Count(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()));
                index.doneIssueCount = context.Issues.Count(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString()));
                return View(index);
            }
            else
            {
                return returnToLogin();
            };
        }

        #endregion

        #region Profile
        [AuthFilter(PageName = "profile")]
        public ActionResult ProfilePage()
        {
            if (reloadViewBagFromSession("Kullanıcı Profili", EnumClass.Page.profile))
            {
                context = external.RefreshAll(context);
                int logged_user_id = ViewBag.logged_user.id;
                return View(context.Users.FirstOrDefault(x => x.ID == logged_user_id));
            }
            else
            {
                return returnToLogin();
            };

        }

        [HttpPost, AuthFilter(PageName = "profile")]
        public ActionResult ProfilePage(User user)
        {
            if (reloadViewBagFromSession("Kullanıcı Profili", EnumClass.Page.profile))
            {
                context = external.RefreshAll(context);
                User updatedUser = context.Users.FirstOrDefault(x => x.ID == user.ID);
                if (updatedUser.Password != user.Password)
                {
                    updatedUser.Password = external.GenerateSha256(user.Password);
                    updatedUser.PasswordAgain = updatedUser.Password;
                }
                updatedUser.Email = user.Email;
                updatedUser.Person.Phone = user.Person.Phone;
                updatedUser.Person.Adres = user.Person.Adres;
                updatedUser.Person.Name = user.Person.Name;
                updatedUser.Person.SurName = user.Person.SurName;
                context.SaveChanges();
                new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress() { Email = updatedUser.Email, Name = updatedUser.Person.Name }, EnumClass.MailType.congratulation, title: "Tebrikler", text: "Hesap Bilgilerin Başarıyla Güncellendi");
                setProcess(EnumClass.ProcessType.success, "Kullanıcı Bilgileri Başarıyla Değiştirildi.");
                return View(updatedUser);
            }
            else
            {
                return returnToLogin();
            };


        }
        #endregion

        #region Issues
        [AuthFilter(PageName = "issues")]
        public ActionResult Issues(Issue issue = null, EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "", string @enumStr = "initialize")
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                setProcess(@enum, processMessage);
                if ((EnumClass.ProcessType)Enum.Parse(typeof(EnumClass.ProcessType), enumStr, true) == EnumClass.ProcessType.@new)
                {
                    ViewBag.issuePanel = true;
                }
                if (issue == null || (issue.ID == 0 && issue.IssueText == null && issue.IssueTitle == null))
                {
                    issue = new Issue() { Priority = -1 };
                }
                if (TempData["activeNavBar"] == null)
                {
                    TempData["activeNavBar"] = "waitingNav";
                }
                ViewBag.issueList = getIssueList();
                ViewBag.employeeList = getEmployeeListForDropDown();
                ViewBag.userType = ViewBag.logged_user.userType;
                ViewBag.userID = ViewBag.logged_user.id;
                return View(issue);
            }
            else
            {
                return returnToLogin();
            };
        }

        [HttpPost, AuthFilter(PageName = "issues")]
        [MultipleButton(Name = "action", Argument = "SaveIssue")]
        public ActionResult SaveIssue(Issue issue)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                if (issue != null)
                {
                    if (issue.ID == 0)
                    {
                        int logged_user_id = ViewBag.logged_user.id;
                        if (!String.IsNullOrEmpty(issue.IssueText) && issue.Priority != -1)
                        {
                            if (issue.AppointedUser != null && issue.AppointedUser.ID > 0)
                            {
                                do
                                {
                                    issue.AppointedUser = context.Users.FirstOrDefault(x => x.ID == issue.AppointedUser.ID);
                                    issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Aktif.ToString());
                                    issue.AppointedTime = DateTime.Now;
                                    issue.DemandApproval = true;
                                    issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()));
                                } while (issue.AppointedUser != context.Users.FirstOrDefault(x => x.ID == issue.AppointedUser.ID) || issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Aktif.ToString()) || issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString())) || issue.AppointedTime == null);
                            }
                            else
                            {
                                do
                                {
                                    issue.AppointedUser = null;
                                    issue.AppointedTime = null;
                                    issue.DemandApproval = true;
                                    issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()));
                                    issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Beklemede.ToString());
                                } while (issue.AppointedUser != null || issue.AppointedTime != null || issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Beklemede.ToString()) || issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString())));

                            }
                            do
                            {
                                issue.OpenedBy = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                            } while (issue.OpenedBy != context.Users.FirstOrDefault(x => x.ID == logged_user_id));

                            context.Issues.Add(issue);
                            context.SaveChanges();
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talebiniz Kaydedilmiştir." });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = issue, @enum = EnumClass.ProcessType.warning, processMessage = "Boş Alanları Doldurun." });
                        }
                    }
                    else
                    {
                        Issue editedIssue = context.Issues.FirstOrDefault(x => x.ID == issue.ID);
                        if (issue.AppointedUser != null && issue.AppointedUser.ID > 0)
                        {
                            do
                            {
                                editedIssue.AppointedUser = context.Users.FirstOrDefault(x => x.ID == issue.AppointedUser.ID);
                                editedIssue.Status = context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Aktif.ToString());
                                editedIssue.DemandApproval = true;
                                editedIssue.AppointedTime = DateTime.Now;
                                editedIssue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()));
                            }
                            while (editedIssue.AppointedUser != context.Users.FirstOrDefault(x => x.ID == issue.AppointedUser.ID) || editedIssue.Status != context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Aktif.ToString()) || editedIssue.DemandApproval != true || editedIssue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString())) || editedIssue.AppointedTime == null);

                        }
                        else
                        {
                            do
                            {
                                editedIssue.AppointedUser = null;
                                editedIssue.AppointedTime = null;
                                editedIssue.Status = context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Beklemede.ToString());
                                editedIssue.DemandApproval = true;
                                editedIssue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()));
                            }
                            while (editedIssue.AppointedUser != null || editedIssue.AppointedTime != null || editedIssue.Status != context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Beklemede.ToString()) || editedIssue.DemandApproval != true || editedIssue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString())));
                        }
                        do
                        {
                            editedIssue.IssueTitle = issue.IssueTitle;
                            editedIssue.IssueText = issue.IssueText;
                            editedIssue.Priority = issue.Priority;
                            editedIssue.ClosedBy = null;
                            editedIssue.ClosedTime = null;
                        } while (editedIssue.IssueTitle != issue.IssueTitle || editedIssue.IssueText != issue.IssueText || editedIssue.Priority != issue.Priority || editedIssue.ClosedBy != null || editedIssue.ClosedTime != null);
                        if (context.SaveChanges() > 0)
                        {
                            switch (editedIssue.Status.StatusType)
                            {
                                case "Beklemede":
                                    new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(editedIssue.OpenedBy.Email, editedIssue.OpenedBy.Person.Name + " " + editedIssue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + editedIssue.IssueTitle + "] başlıklı talep [Bekleme] Konumuna Alındı ");
                                    break;
                                case "Aktif":
                                    new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(editedIssue.OpenedBy.Email, editedIssue.OpenedBy.Person.Name + " " + editedIssue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + editedIssue.IssueTitle + "] başlıklı talep [" + editedIssue.AppointedUser.Person.Name + " " + editedIssue.AppointedUser.Person.SurName + "] isimli Çalışana Atandı ve [Aktif] Konumuna Alındı ");
                                    new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(editedIssue.AppointedUser.Email, editedIssue.AppointedUser.Person.Name + " " + editedIssue.AppointedUser.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "[" + editedIssue.IssueTitle + "] başlıklı talebe Atandınız ve Talep [Aktif] Konumuna Alındı.");
                                    break;
                                case "Tamamlandı":
                                    new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(editedIssue.OpenedBy.Email, editedIssue.OpenedBy.Person.Name + " " + editedIssue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + editedIssue.IssueTitle + "] başlıklı talep [" + editedIssue.ClosedBy.Person.Name + " " + editedIssue.ClosedBy.Person.SurName + "] Tarafından [Tamamlandı] Konumuna Alındı ");
                                    new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(editedIssue.AppointedUser.Email, editedIssue.AppointedUser.Person.Name + " " + editedIssue.AppointedUser.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Atanmış Olduğunuz [" + editedIssue.IssueTitle + "] başlıklı talep [" + editedIssue.ClosedBy.Person.Name + " " + editedIssue.ClosedBy.Person.SurName + "] Tarafından [Tamamlandı] Konumuna Alındı ");
                                    break;
                            }

                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talebiniz Güncellenmiştir" });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = issue, @enum = EnumClass.ProcessType.warning, processMessage = "Talebiniz Güncellenemedi. Lütfen Tekrar Deneyin." });
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Talep Kaydedilemedi Lütfen Tekrar Deneyin." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult GetIssue(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                int logged_user_id = ViewBag.logged_user.id;
                User logged_user = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    if (logged_user.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator.ToString()))
                    {
                        do
                        {
                            issue.AppointedUser = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                            issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()));
                            issue.AppointedTime = DateTime.Now;
                            issue.DemandApproval = true;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()));
                        } while (issue.AppointedUser != context.Users.FirstOrDefault(x => x.ID == logged_user_id) || issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Aktif.ToString())) || issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString())) || issue.AppointedTime == null);
                        if (issue.AppointedUser != null)
                        {
                            if (context.SaveChanges() > 0)
                            {
                                new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.OpenedBy.Email, issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + issue.IssueTitle + "] başlıklı talep [" + issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName + "] isimli Çalışana Atandı ve [Aktif] Konumuna Alındı ");
                                new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.AppointedUser.Email, issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "[" + issue.IssueTitle + "] başlıklı talebi Üstlendiniz ve Talep [Aktif] Konumuna Alındı.");
                                return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talebi Üstlendiniz." });
                            }
                            else
                            {
                                return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız. Lütfen Tekrar Deneyin." });
                            }
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız. Lütfen Tekrar Deneyin." });
                        }
                    }
                    else
                    {
                        do
                        {
                            issue.AppointedUser = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                            issue.DemandApproval = false;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()));
                        } while (issue.AppointedUser != context.Users.FirstOrDefault(x => x.ID == logged_user_id) || issue.DemandApproval != false || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString())));
                        if (context.SaveChanges() > 0)
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talep Üstlenme İsteğiniz Alındı. Yönetici Onayı Bekleniyor." });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız. Lütfen Tekrar Deneyin." });
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult LeftIssue(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                int logged_user_id = ViewBag.logged_user.id;
                User logged_user = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    string appointedUserMail = issue.AppointedUser.Email,
                        appointedUserFullName = issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName;
                    if (logged_user.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator.ToString()))
                    {
                        do
                        {
                            issue.AppointedUser = null;
                            issue.AppointedTime = null;
                            issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString()));
                            issue.DemandApproval = true;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()));
                        } while (issue.AppointedUser != null || issue.AppointedTime != null || issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())) || issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString())));
                        if (context.SaveChanges() > 0)
                        {
                            new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.OpenedBy.Email, issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + issue.IssueTitle + "] başlıklı talep [Bekleme] Konumuna Alındı ");
                            new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(appointedUserMail, appointedUserFullName), EnumClass.MailType.congratulation, "Bilgilendirme", "[" + issue.IssueTitle + "] başlıklı talebi Bıraktınız ve Talep [Bekleme] Konumuna Alındı.");
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talebi Bıraktınız." });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız. Lütfen Tekrar Deneyin." });
                        }
                    }
                    else
                    {
                        do
                        {
                            issue.DemandApproval = false;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()));
                        } while (issue.DemandApproval != false || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString())));
                        if (context.SaveChanges() > 0)
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talep Bırakma İsteğiniz Alındı. Yönetici Onayı Bekleniyor." });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız. Lütfen Tekrar Deneyin." });
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult AllowIssueRequest(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                int logged_user_id = ViewBag.logged_user.id;
                User logged_user = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    string userMessage = "", employeeMessage = "",
                        appointedName = issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName, appointeMail = issue.AppointedUser.Email;

                    if (issue.RequestType.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()))
                    {
                        do
                        {
                            issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()));
                            issue.AppointedTime = DateTime.Now;
                            issue.DemandApproval = true;
                        } while (issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Aktif.ToString())) || issue.DemandApproval != true || issue.AppointedTime == null);
                        employeeMessage = "[" + issue.IssueTitle + "] Başlıklı Talebi Üstlenme İsteğiniz Yönetici Tarafından Kabul Edildi ve Talep [Aktif] Konumuna Alındı.";
                        userMessage = "Açmış Olduğunuz [" + issue.IssueTitle + "] Başlıklı Talep [" + issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName + "] İsimli Çalışana Atandı ve [Aktif] Konumuna Alındı ";
                    }
                    else if (issue.RequestType.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()))
                    {
                        employeeMessage = "[" + issue.IssueTitle + "] Başlıklı Talebi Bırakma İsteğiniz Yönetici Tarafından Kabul Edildi.";
                        userMessage = "Açmış Olduğunuz [" + issue.IssueTitle + "] Başlıklı Talep [" + issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName + "] İsimli Çalışandan Alındı. Yeni Bir Atama Gerçekleştirilene Kadar Lütfen Bekleyin.";
                        do
                        {
                            issue.AppointedUser = null;
                            issue.AppointedTime = null;
                            issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString()));
                            issue.DemandApproval = true;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()));
                        } while (issue.AppointedUser != null || issue.AppointedTime != null || issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())) || issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString())));
                    }
                    if (context.SaveChanges() > 0)
                    {
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.OpenedBy.Email, issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", userMessage);
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(appointeMail, appointedName), EnumClass.MailType.congratulation, "Bilgilendirme", employeeMessage);

                        return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "İstek Başarıyla Onaylandı" });
                    }
                    else
                    {
                        return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız Lütfen Tekrar Deneyin." });
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult DenyIssueRequest(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                int logged_user_id = ViewBag.logged_user.id;
                User logged_user = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    string employeeMessage = "",
                        appointedName = issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName, appointeMail = issue.AppointedUser.Email,
                        denyText = logged_user.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator.ToString()) ? "Yönetici Tarafından Red" : "İptal";

                    if (issue.RequestType.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()))
                    {
                        do
                        {
                            issue.DemandApproval = true;
                            issue.AppointedUser = null;
                        } while (issue.AppointedUser != null || issue.DemandApproval != true);
                        employeeMessage = "[" + issue.IssueTitle + "] Başlıklı Talebi Üstlenme İsteğiniz " + denyText + " Edildi.";
                    }
                    else if (issue.RequestType.RequestTypeText.Equals(EnumClass.RequestType.cancel.ToString()))
                    {
                        do
                        {
                            issue.DemandApproval = true;
                            issue.RequestType = context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString()));
                        } while (issue.DemandApproval != true || issue.RequestType != context.RequestTypes.FirstOrDefault(x => x.RequestTypeText.Equals(EnumClass.RequestType.get.ToString())));
                        employeeMessage = "[" + issue.IssueTitle + "] Başlıklı Talebi Bırakma İsteğiniz " + denyText + " Edildi.";
                    }
                    if (context.SaveChanges() > 0)
                    {
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(appointeMail, appointedName), EnumClass.MailType.congratulation, "Bilgilendirme", employeeMessage);
                        return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "İstek Başarıyla İptal Edildi." });
                    }
                    else
                    {
                        return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "İşlem Başarısız Lütfen Tekrar Deneyin." });
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }

        [AuthFilter(PageName = "issues")]
        public ActionResult SelectIssueForEdit(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                TempData["editIssue"] = true;
                ViewBag.issueList = getIssueList();
                ViewBag.employeeList = getEmployeeListForDropDown();
                ViewBag.userType = ViewBag.logged_user.userType;
                ViewBag.userID = ViewBag.logged_user.id;
                ViewBag.issuePanel = true;

                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    return View("Issues", issue);
                }
                else
                {
                    return View("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }

        [AuthFilter(PageName = "issues")]
        public ActionResult RemoveIssue(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                string openedMail = "", openedName = "", appointedMail = "", appointedName = "", closedMail = "", closedName = "", issueTitle = "";
                if (issue != null)
                {
                    issueTitle = issue.IssueTitle;
                    if (!String.IsNullOrEmpty(issueTitle) && issue.OpenedBy != null)
                    {
                        openedMail = issue.OpenedBy.Email;
                        openedName = issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName;
                        if (issue.AppointedUser != null)
                        {
                            appointedMail = issue.AppointedUser.Email;
                            appointedName = issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName;
                        }
                        if (issue.ClosedBy != null)
                        {
                            closedMail = issue.ClosedBy.Email;
                            closedName = issue.ClosedBy.Person.Name + " " + issue.ClosedBy.Person.SurName;
                        }


                        context.Issues.Remove(issue);
                        if (context.SaveChanges() > 0)
                        {
                            new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(openedMail, openedName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + issueTitle + "] başlıklı talep Silindi");

                            if (!String.IsNullOrEmpty(appointedMail) && !String.IsNullOrEmpty(appointedName))
                            {
                                new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(appointedMail, appointedName), EnumClass.MailType.congratulation, "Bilgilendirme", "Atanmış Olduğunuz [" + issueTitle + "] başlıklı talep Silindi");
                            }
                            if (!String.IsNullOrEmpty(closedName) && !String.IsNullOrEmpty(closedMail))
                            {
                                new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(closedMail, closedName), EnumClass.MailType.congratulation, "Bilgilendirme", "[Tamamlandı] Konumuna Aldığınız [" + issueTitle + "] başlıklı talep Silindi");
                            }
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Kayıt Başarıyla Silindi" });
                        }
                        else
                        {
                            return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.danger, processMessage = "Kayıt Silinemedi. Lütfen Tekrar Deneyiniz." });
                        }
                    }
                    else
                    {
                        return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                    }
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        public List<Issue> getIssueList()
        {
            List<Issue> issueList = new List<Issue>();
            int logged_user_id = ViewBag.logged_user.id;
            switch (ViewBag.logged_user.userType)
            {
                case "Administrator": issueList = context.Issues.OrderByDescending(x => x.ID).ToList(); break;
                case "Customer": issueList = context.Issues.OrderByDescending(x => x.ID).Where(x => x.OpenedBy.ID == logged_user_id).ToList(); break;
                case "Employee": issueList = context.Issues.OrderByDescending(x => x.ID).Where(x => x.OpenedBy.ID == logged_user_id || x.AppointedUser.ID == logged_user_id || x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())).ToList(); break;
                default: issueList = new List<Issue>(); break;
            }
            ViewBag.waitingIssueList = issueList.Where(x => x.Status.StatusType == "Beklemede" && (x.DemandApproval || (x.AppointedUser != null && x.AppointedUser.ID == logged_user_id) || x.OpenedBy.ID == logged_user_id)).ToList();
            ViewBag.activeIssueList = issueList.Where(x => x.Status.StatusType == "Aktif").ToList();
            ViewBag.doneIssueList = issueList.Where(x => x.Status.StatusType == "Tamamlandı").ToList();
            ViewBag.notApprovedIssueList = issueList.Where(x => !x.DemandApproval).ToList();

            return issueList;
        }

        public List<SelectListItem> getEmployeeListForDropDown(string firstRow = "Çalışan Seçin")
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem() { Text = firstRow, Value = "-1" });
            foreach (var item in context.Users.Where(x => x.UserType.UserTypeText != EnumClass.UserType.Customer.ToString()).ToList())
            {
                selectListItems.Add(new SelectListItem() { Text = item.Username, Value = item.ID.ToString() });
            }

            return selectListItems;
        }

        public List<SelectListItem> getCustomerListForDropDown(string firstRow = "Müşteri Seçin")
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem() { Text = firstRow, Value = "-1" });
            foreach (var item in context.Users.Where(x => x.UserType.UserTypeText == EnumClass.UserType.Customer.ToString()).ToList())
            {
                selectListItems.Add(new SelectListItem() { Text = item.Username, Value = item.ID.ToString() });
            }

            return selectListItems;
        }

        public List<SelectListItem> getStatusListForDropDown(string firstRow = "Statü Seçin")
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem() { Text = firstRow, Value = "-1" });
            foreach (var item in context.Statuses.ToList())
            {
                selectListItems.Add(new SelectListItem() { Text = item.StatusType, Value = item.ID.ToString() });
            }

            return selectListItems;
        }
        #endregion

        #region IssueDetails
        [AuthFilter(PageName = "issues")]
        public ActionResult IssueDetail(int ID = 0, EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "")
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                if (ID > 0)
                {
                    setProcess(@enum, processMessage);
                    ViewBag.IssueFileList = getIssueFileList(ID);
                    ViewBag.IssueDetailList = getIssueDetailList(ID);
                    ViewBag.userNameSurname = ViewBag.logged_user.userNameSurname;
                    ViewBag.userType = ViewBag.logged_user.userType;
                    ViewBag.userID = ViewBag.logged_user.id;
                    TempData["issueID"] = ID;
                    Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                    IssueDetailViewModel issueDetailView = new IssueDetailViewModel()
                    {
                        IssueID = issue.ID,
                        issueTitle = issue.IssueTitle,
                        IssueText = issue.IssueText,
                        UserID = issue.OpenedBy.ID,
                        UserName = issue.OpenedBy.Username,
                        UserNameSurname = issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName,
                        AppointedUserID = issue.AppointedUser != null ? issue.AppointedUser.ID : 0,
                        AppointedUser = issue.AppointedUser != null ? issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName : "Belirlenmedi",
                        DemandApproval = issue.DemandApproval,
                        CreatedTime = issue.CreatedTime,
                        issueDetailID = 0,
                        Message = ""
                    };
                    return View(issueDetailView);
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            }
        }

        [HttpPost, AuthFilter(PageName = "issues")]
        public ActionResult IssueDetail(IssueDetailViewModel issueDetailView)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                if (issueDetailView.IssueID > 0 && !String.IsNullOrEmpty(issueDetailView.Message))
                {
                    int logged_user_id = (int)ViewBag.logged_user.id;
                    Issue issue = context.Issues.FirstOrDefault(x => x.ID == issueDetailView.IssueID);
                    IssueDetail issueDetail = new IssueDetail();
                    issueDetail.Issue = issue;
                    issueDetail.User = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                    issueDetail.Message = issueDetailView.Message;
                    context.IssueDetails.Add(issueDetail);
                    if (logged_user_id != issue.OpenedBy.ID)
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.OpenedBy.Email, issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Açmış Olduğunuz [" + issue.IssueTitle + "] başlıklı talebe [" + issueDetail.User.Person.Name + " " + issueDetail.User.Person.SurName + "] Tarafından yorum yapıldı");
                    if (issue.AppointedUser != null && logged_user_id != issue.AppointedUser.ID)
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.AppointedUser.Email, issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "Atanmış Olduğunuz [" + issue.IssueTitle + "] başlıklı talebe [" + issueDetail.User.Person.Name + " " + issueDetail.User.Person.SurName + "] Tarafından yorum yapıldı");
                    if (issue.ClosedBy != null && logged_user_id != issue.ClosedBy.ID)
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(issue.ClosedBy.Email, issue.ClosedBy.Person.Name + " " + issue.ClosedBy.Person.SurName), EnumClass.MailType.congratulation, "Bilgilendirme", "[Tamamlandı] konumuna aldığınız [" + issue.IssueTitle + "] başlıklı talebe [" + issueDetail.User.Person.Name + " " + issueDetail.User.Person.SurName + "] Tarafından yorum yapıldı");

                    context.SaveChanges();
                    return RedirectToAction("IssueDetail", new { ID = issueDetailView.IssueID, @enum = EnumClass.ProcessType.success, processMessage = "Yorum Eklendi" });
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            }
        }

        public FileResult DownloadFile(string fileName)
        {
            var FileVirtualPath = "~/Content/uploads/" + fileName;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }

        [HttpPost, AuthFilter(PageName = "issues")]
        public ActionResult UploadFileForIssue(IEnumerable<HttpPostedFileBase> ChosenFiles)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                HttpPostedFileBase imagesx = Request.Files["ChosenFiles"];
                context = external.RefreshAll(context);
                int id = TempData["issueID"] != null ? int.Parse(TempData["issueID"].ToString()) : 0;
                int logged_user_id = ViewBag.logged_user.id;
                User user = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == id);
                foreach (var file in ChosenFiles)
                {
                    if (file.ContentLength > 0)
                    {
                        string guid = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName).ToLower();
                        file.SaveAs(Server.MapPath("~/Content/uploads/" + guid));
                        IssueDetail issueDetail = new IssueDetail()
                        {
                            Issue = issue,
                            CreatedTime = DateTime.Now,
                            FileName = guid,
                            Message = "-",
                            User = user
                        };
                        context.IssueDetails.Add(issueDetail);
                    }
                }
                if (context.SaveChanges() > 0)
                {
                    return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.success, processMessage = "Dosya Başarıyla Yüklendi." });
                }
                else
                {
                    return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.warning, processMessage = "Dosya Yüklenemedi." });
                }
            }
            else
            {
                return returnToLogin();
            }
        }

        [AuthFilter(PageName = "issues")]
        public ActionResult DeleteFileForIssue(string guid = "-")
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                int id = TempData["issueID"] != null ? int.Parse(TempData["issueID"].ToString()) : 0;
                string fullPath = Request.MapPath("~/Content/uploads/" + guid);
                bool delete = false;
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                    delete = true;
                }
                else
                {
                    return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.warning, processMessage = "Dosya Silinemedi." });
                }
                if (delete)
                {
                    IssueDetail issueDetail = context.IssueDetails.FirstOrDefault(x => x.FileName.Equals(guid) && x.Message.Equals("-"));
                    context.IssueDetails.Remove(issueDetail);
                    if (context.SaveChanges() > 0)
                    {
                        return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.success, processMessage = "Dosya Başarıyla Silindi." });
                    }
                    else
                    {
                        return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.warning, processMessage = "Dosya Silinemedi." });
                    }
                }
                else
                {
                    return RedirectToAction("IssueDetail", new { ID = id, @enum = EnumClass.ProcessType.warning, processMessage = "Dosya Silinemedi." });
                }
            }
            else
            {
                return returnToLogin();
            }
        }

        [AuthFilter(PageName = "issues")]
        public ActionResult SelectIssueDetailForEdit(int ID, EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "")
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                if (ID > 0)
                {
                    setProcess(@enum, processMessage);
                    IssueDetail issueDetail = context.IssueDetails.FirstOrDefault(x => x.ID == ID);
                    Issue issue = issueDetail.Issue;
                    TempData["issueID"] = issue.ID;
                    ViewBag.IssueFileList = getIssueFileList(issue.ID);
                    ViewBag.IssueDetailList = getIssueDetailList(issue.ID);
                    ViewBag.userNameSurname = ViewBag.logged_user.userNameSurname;
                    ViewBag.userType = ViewBag.logged_user.userType;
                    ViewBag.userID = ViewBag.logged_user.id;
                    ViewBag.editIssue = true;
                    IssueDetailViewModel issueDetailView = new IssueDetailViewModel()
                    {
                        IssueID = issue.ID,
                        issueTitle = issue.IssueTitle,
                        IssueText = issue.IssueText,
                        UserName = issue.OpenedBy.Username,
                        UserNameSurname = issue.OpenedBy.Person.Name + " " + issue.OpenedBy.Person.SurName,
                        AppointedUser = issue.AppointedUser != null ? issue.AppointedUser.Person.Name + " " + issue.AppointedUser.Person.SurName : "Belirlenmedi",
                        CreatedTime = issue.CreatedTime,
                        issueDetailID = issueDetail.ID,
                        Message = issueDetail.Message
                    };
                    return View("IssueDetail", issueDetailView);
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            }
        }


        [HttpPost, AuthFilter(PageName = "issues")]
        [MultipleButton(Name = "action", Argument = "UpdateIssueDetail")]
        public ActionResult UpdateIssueDetail(IssueDetailViewModel issueDetailViewModel)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                if (issueDetailViewModel != null && issueDetailViewModel.issueDetailID > 0)
                {
                    int logged_user_id = ViewBag.logged_user.id;
                    IssueDetail issueDetail = context.IssueDetails.FirstOrDefault(x => x.ID == issueDetailViewModel.issueDetailID);
                    issueDetail.Message = issueDetailViewModel.Message;
                    if (context.SaveChanges() > 0)
                    {
                        return RedirectToAction("IssueDetail", new { ID = issueDetailViewModel.IssueID, @enum = EnumClass.ProcessType.success, processMessage = "Yorumunuz Güncellenmiştir." });
                    }
                    else
                    {
                        return RedirectToAction("SelectIssueDetailForEdit", new { ID = issueDetailViewModel.issueDetailID, @enum = EnumClass.ProcessType.warning, processMessage = "Yorumunuz Güncellenemedi." });
                    }
                }
                else
                {
                    return RedirectToAction("SelectIssueDetailForEdit", new { ID = issueDetailViewModel.issueDetailID, @enum = EnumClass.ProcessType.warning, processMessage = "Yorumunuz Güncellenemedi. Lütfen Tekrar Deneyin." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult RemoveIssueDetail(int ID)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                IssueDetail issueDetail = context.IssueDetails.FirstOrDefault(x => x.ID == ID);
                if (issueDetail != null)
                {
                    int issueID = issueDetail.Issue.ID;
                    context.IssueDetails.Remove(issueDetail);
                    if (context.SaveChanges() > 0)
                    {
                        return RedirectToAction("IssueDetail", new { ID = issueID, @enum = EnumClass.ProcessType.success, processMessage = "Kayıt Başarıyla Silindi" });
                    }
                    else
                    {
                        return RedirectToAction("IssueDetail", new { ID = issueID, @enum = EnumClass.ProcessType.danger, processMessage = "Kayıt Silinemedi. Lütfen Tekrar Deneyiniz." });
                    }
                }
                else
                {
                    return RedirectToAction("IssueDetail", new { ID = 0, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [AuthFilter(PageName = "issues")]
        public ActionResult CloseIssue(int ID = 0)
        {
            if (reloadViewBagFromSession("Talepler", EnumClass.Page.issues))
            {
                context = external.RefreshAll(context);
                Issue issue = context.Issues.FirstOrDefault(x => x.ID == ID);
                if (issue != null)
                {
                    int logged_user_id = (int)ViewBag.logged_user.id;
                    do
                    {
                        issue.Status = context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Tamamlandı.ToString());
                        issue.ClosedBy = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                        issue.ClosedTime = DateTime.Now;
                    } while (issue.Status != context.Statuses.FirstOrDefault(x => x.StatusType == EnumClass.StatusType.Tamamlandı.ToString()) || issue.ClosedBy != context.Users.FirstOrDefault(x => x.ID == logged_user_id) || issue.ClosedTime == null);

                    context.SaveChanges();
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.success, processMessage = "Talep Başarıyla Kapatıldı" });
                }
                else
                {
                    return RedirectToAction("Issues", new { issue = new Issue() { Priority = -1 }, @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }

        public List<IssueDetail> getIssueDetailList(int? issueID)
        {
            issueID = issueID == null ? 0 : issueID;
            context = external.RefreshAll(context);
            List<IssueDetail> issueDetails = context.IssueDetails.OrderBy(x => x.ID).Where(x => x.Issue.ID == issueID && String.IsNullOrEmpty(x.FileName)).ToList();
            return issueDetails;
        }


        public List<IssueDetail> getIssueFileList(int? issueID)
        {
            issueID = issueID == null ? 0 : issueID;
            context = external.RefreshAll(context);
            List<IssueDetail> issueDetails = context.IssueDetails.OrderBy(x => x.ID).Where(x => x.Issue.ID == issueID && !String.IsNullOrEmpty(x.FileName)).ToList();
            return issueDetails;
        }
        #endregion

        #region Users
        [AuthFilter(PageName = "users")]
        public ActionResult Users(EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "")
        {
            if (reloadViewBagFromSession("Kullanıcılar", EnumClass.Page.users))
            {
                setProcess(@enum, processMessage);
                ViewBag.userType = ViewBag.logged_user.userType;
                context = external.RefreshAll(context);
                List<User> userList = context.Users.ToList();
                ViewBag.UserList = userList;
                return View();
            }
            else
            {
                return returnToLogin();
            };
        }

        #region RegisterPage
        [AuthFilter(PageName = "register")]
        public ActionResult RegisterPage(int ID = 0)
        {
            if (reloadViewBagFromSession("Kayıt Oluştur", EnumClass.Page.users))
            {
                context = external.RefreshAll(context);

                var ContractTypeList = context.ContractTypes.Select(x => new SelectListItem { Value = x.ID.ToString(), Text = x.ContractTypeText });
                ViewBag.ContractTypeList = new SelectList(ContractTypeList, "Value", "Text");

                ViewBag.UserTypeList = getUserTypeListForDropDown();
                RegisterPageViewModel userView = new RegisterPageViewModel();
                if (ID == 0)
                {
                    userView.User = new User();
                    userView.Person = new Person();
                    userView.Customer = new Customer();
                    userView.contractEndTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                else
                {
                    userView.User = context.Users.FirstOrDefault(x => x.ID == ID);
                    userView.Person = userView.User.Person;

                    if (userView.User.UserType.UserTypeText.Equals(EnumClass.UserType.Customer.ToString()))
                    {
                        userView.Customer = context.Customers.FirstOrDefault(x => x.Person.ID == userView.Person.ID);
                    }
                    if (userView.Customer != null)
                    {
                        userView.contractEndTime = userView.Customer.ContractEndTime.ToString("dd/MM/yyyy").Replace(".", "/");
                    }
                    else
                    {
                        userView.Customer = new Customer();
                        userView.contractEndTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                    }
                    if (userView.User == null || userView.Person == null || userView.Customer == null)
                    {
                        return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.warning, processMessage = "Kullanıcı Bulunamadı." });
                    }
                    ViewBag.EditUser = true;
                }
                return View(userView);
            }
            else
            {
                return returnToLogin();
            };
        }

        [HttpPost, AuthFilter(PageName = "register")]
        [MultipleButton(Name = "action", Argument = "CreateUser")]
        public ActionResult CreateUser(RegisterPageViewModel userView)
        {
            if (reloadViewBagFromSession("Kayıt Oluştur", EnumClass.Page.users))
            {
                context = external.RefreshAll(context);

                var ContractTypeList = context.ContractTypes.Select(x => new SelectListItem { Value = x.ID.ToString(), Text = x.ContractTypeText });
                ViewBag.ContractTypeList = new SelectList(ContractTypeList, "Value", "Text");

                ViewBag.UserTypeList = getUserTypeListForDropDown();

                if (userView != null && userView.User != null && userView.Person != null && userView.Customer != null)
                {
                    Person person = null;
                    User user = null;
                    Customer customer = null;

                    if (!String.IsNullOrEmpty(userView.Person.Name) && !String.IsNullOrEmpty(userView.Person.SurName) &&
                        !String.IsNullOrEmpty(userView.User.Username) && userView.User.Password == userView.User.PasswordAgain && !String.IsNullOrEmpty(userView.User.Email))
                    {
                        if (context.Users.FirstOrDefault(x => x.Username.Equals(userView.User.Username)) != null)
                        {
                            ModelState.AddModelError("", "Bu Kullanıcı Adı Kullanılıyor");
                            return View("RegisterPage", userView);
                        }
                        if (context.Users.FirstOrDefault(x => x.Email.Equals(userView.User.Email)) != null)
                        {
                            ModelState.AddModelError("", "Bu Mail Adresi Sistemde Mevcut");
                            return View("RegisterPage", userView);
                        }

                        person = new Person()
                        {
                            Name = userView.Person.Name,
                            SurName = userView.Person.SurName,
                            Phone = userView.Person.Phone,
                            Adres = userView.Person.Adres
                        };
                        context.People.Add(person);
                        user = new User()
                        {
                            Username = userView.User.Username,
                            Password = external.GenerateSha256(userView.User.Password),
                            PasswordAgain = external.GenerateSha256(userView.User.Password),
                            Email = userView.User.Email,
                            UserType = context.UserTypes.FirstOrDefault(x => x.ID == userView.User.UserType.ID),
                            CreatedTime = DateTime.Now,
                            Person = person
                        };
                        context.Users.Add(user);
                    }

                    if (userView.User.UserType.ID == context.UserTypes.FirstOrDefault(x => x.UserTypeText.Equals(EnumClass.UserType.Customer.ToString())).ID)
                    {
                        customer = new Customer()
                        {
                            Person = person,
                            CustomerCode = userView.Customer.CustomerCode,
                            ManagerPhone = userView.Customer.ManagerPhone,
                            ContractType = context.ContractTypes.FirstOrDefault(x => x.ID == userView.Customer.ContractType.ID),
                            ContractEndTime = DateTime.Parse(userView.contractEndTime)
                        };
                        context.Customers.Add(customer);
                    }
                    if (context.SaveChanges() > 0)
                    {
                        new Sendgrid().SendEMail(new SendGrid.Helpers.Mail.EmailAddress(user.Email, person.Name + " " + person.SurName), EnumClass.MailType.welcome);
                        return RedirectToRoute("Users", new { @enum = EnumClass.ProcessType.success, processMessage = "Kayıt başarılı." });
                    }
                    else
                    {
                        setProcess(EnumClass.ProcessType.warning, "Kayıt Yapılamadı Lütfen Tekrar Deneyin.");
                        return View("RegisterPage", userView);
                    }

                }
                else
                {
                    setProcess(Models.Enums.EnumClass.ProcessType.warning, "Kayıt Yapılamadı Lütfen Tekrar Deneyin.");
                    return View("RegisterPage", new RegisterPageViewModel() { User = new User(), Person = new Person(), Customer = new Customer(), contractEndTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        [HttpPost, AuthFilter(PageName = "register")]
        [MultipleButton(Name = "action", Argument = "UpdateUser")]
        public ActionResult UpdateUser(RegisterPageViewModel userView)
        {
            if (reloadViewBagFromSession("Kayıt Oluştur", EnumClass.Page.users))
            {
                ViewBag.EditUser = true;
                context = external.RefreshAll(context);

                var ContractTypeList = context.ContractTypes.Select(x => new SelectListItem { Value = x.ID.ToString(), Text = x.ContractTypeText });
                ViewBag.ContractTypeList = new SelectList(ContractTypeList, "Value", "Text");

                ViewBag.UserTypeList = getUserTypeListForDropDown();

                if (userView != null && userView.User != null && userView.Person != null && userView.Customer != null)
                {
                    Person person = null;
                    User user = null;
                    Customer customer = null;

                    if (!String.IsNullOrEmpty(userView.Person.Name) && !String.IsNullOrEmpty(userView.Person.SurName) &&
                        !String.IsNullOrEmpty(userView.User.Username) && userView.User.Password == userView.User.PasswordAgain && !String.IsNullOrEmpty(userView.User.Email))
                    {
                        if (context.Users.FirstOrDefault(x => x.Username.Equals(userView.User.Username) && x.ID != userView.User.ID) != null)
                        {
                            ModelState.AddModelError("", "Bu Kullanıcı Adı Kullanılıyor");
                            return View("RegisterPage", userView);
                        }
                        if (context.Users.FirstOrDefault(x => x.Email.Equals(userView.User.Email) && x.ID != userView.User.ID) != null)
                        {
                            ModelState.AddModelError("", "Bu Mail Adresi Sistemde Mevcut");
                            return View("RegisterPage", userView);
                        }

                        user = context.Users.FirstOrDefault(x => x.ID == userView.User.ID);
                        person = context.People.FirstOrDefault(x => x.ID == userView.Person.ID);

                        do
                        {
                            person.Name = userView.Person.Name;
                            person.SurName = userView.Person.SurName;
                            person.Phone = userView.Person.Phone;
                            person.Adres = userView.Person.Adres;
                            user.Username = userView.User.Username;
                            user.Email = userView.User.Email;
                            user.UserType = context.UserTypes.FirstOrDefault(x => x.ID == userView.User.UserType.ID);
                            user.Person = person;

                        } while (person.Name != userView.Person.Name || person.SurName != userView.Person.SurName || person.Phone != userView.Person.Phone || person.Adres != userView.Person.Adres ||
                        user.Username != userView.User.Username || user.Email != userView.User.Email || user.UserType != context.UserTypes.FirstOrDefault(x => x.ID == userView.User.UserType.ID) || user.Person != person);


                        if (!external.GenerateSha256(userView.User.Password).Equals(user.Password) && !userView.User.Password.Equals(user.Password))
                        {
                            do
                            {
                                user.Password = external.GenerateSha256(userView.User.Password);
                                user.PasswordAgain = external.GenerateSha256(userView.User.Password);
                            } while (user.Password != external.GenerateSha256(userView.User.Password) || user.PasswordAgain != external.GenerateSha256(userView.User.Password));
                        }

                    }
                    if (userView.Customer != null)
                        customer = context.Customers.FirstOrDefault(x => x.ID == userView.Customer.ID);

                    if (userView.User.UserType.ID == context.UserTypes.FirstOrDefault(x => x.UserTypeText.Equals(EnumClass.UserType.Customer.ToString())).ID)
                    {
                        bool newCustomer = false;
                        if (customer == null)
                        {
                            customer = new Customer();
                            newCustomer = true;
                        }
                        do
                        {
                            customer.Person = person;
                            customer.CustomerCode = userView.Customer.CustomerCode;
                            customer.ManagerPhone = userView.Customer.ManagerPhone;
                            customer.ContractType = context.ContractTypes.FirstOrDefault(x => x.ID == userView.Customer.ContractType.ID);
                            customer.ContractEndTime = DateTime.Parse(userView.contractEndTime);
                        } while (customer.Person != person || customer.CustomerCode != userView.Customer.CustomerCode || customer.ManagerPhone != userView.Customer.ManagerPhone || customer.ContractType != context.ContractTypes.FirstOrDefault(x => x.ID == userView.Customer.ContractType.ID) || customer.ContractEndTime != DateTime.Parse(userView.contractEndTime));
                        if (newCustomer)
                        {
                            context.Customers.Add(customer);
                        }
                    }
                    else
                    {
                        if (customer != null)
                            context.Customers.Remove(customer);
                    }
                    if (context.SaveChanges() > 0)
                    {
                        return RedirectToRoute("Users", new { @enum = EnumClass.ProcessType.success, processMessage = "Güncelleme başarılı" });
                    }
                    else
                    {
                        setProcess(Models.Enums.EnumClass.ProcessType.warning, "Kayıt Güncellenemedi Lütfen Tekrar Deneyin.");
                        return View("RegisterPage", userView);
                    }

                }
                else
                {
                    return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.warning, processMessage = "Kayıt Bulunamadı Lütfen Tekrar Deneyin." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }
        #endregion

        [AuthFilter(PageName = "users")]
        public ActionResult RemoveUser(int ID)
        {
            if (reloadViewBagFromSession("Kullanıcılar", EnumClass.Page.users))
            {
                User user = context.Users.FirstOrDefault(x => x.ID == ID);
                Person person = context.People.FirstOrDefault(x => x.ID == user.Person.ID);
                if (user != null && person != null)
                {
                    if (user.UserType.UserTypeText.Equals(EnumClass.UserType.Customer.ToString()))
                    {
                        Customer customer = context.Customers.FirstOrDefault(x => x.Person.ID == person.ID);
                        if (customer != null)
                        {
                            context.Customers.Remove(customer);
                        }
                        else
                        {
                            return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                        }
                    }
                    List<Issue> issue = context.Issues.Where(x => x.OpenedBy.ID == user.ID || x.AppointedUser.ID == user.ID || x.ClosedBy.ID == user.ID).ToList();
                    List<IssueDetail> issueDetail = context.IssueDetails.Where(x => x.User.ID == user.ID).ToList();
                    bool thereIsResult = (issue != null && issue.Count > 0) || (issueDetail != null && issueDetail.Count > 0) ? true : false;
                    User adminUser = context.Users.FirstOrDefault(x => x.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator.ToString()));
                    foreach (var item in issue)
                    {
                        if (item.OpenedBy != null && item.OpenedBy.ID == user.ID)
                        {
                            item.OpenedBy = adminUser;
                        }
                        if (item.AppointedUser != null && item.AppointedUser.ID == user.ID)
                        {
                            item.AppointedUser = adminUser;
                        }
                        if (item.ClosedBy != null && item.ClosedBy.ID == user.ID)
                        {
                            item.ClosedBy = adminUser;
                        }
                    }
                    foreach (var item in issueDetail)
                    {
                        if (item.User.ID == user.ID)
                        {
                            item.User = adminUser;
                        }
                    }
                    context.Users.Remove(user);
                    context.People.Remove(person);
                    if (context.SaveChanges() > 0)
                    {
                        string processMessage = thereIsResult ? "Kayıt Başarıyla Silindi. \nBu Kullanıcının Oluşturduğu Talepler ve Yorumlar {" + adminUser.Username + "} Kullanıcısına Atandı." : "Kayıt Başarıyla Silindi.";
                        return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.success, processMessage = processMessage });
                    }
                    else
                    {
                        return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.warning, processMessage = "Kayıt Silme İşlemi Başarısız. Lütfen Tekrar Deneyin." });
                    }
                }
                else
                {
                    return RedirectToAction("Users", new { @enum = EnumClass.ProcessType.warning, processMessage = "Bu Kayıta Ulaşılamıyor." });
                }
            }
            else
            {
                return returnToLogin();
            };
        }


        public List<SelectListItem> getUserTypeListForDropDown()
        {
            int logged_user_id = ViewBag.logged_user.id;
            List<SelectListItem> selectListItems = new List<SelectListItem>();

            foreach (var item in context.UserTypes.ToList())
            {
                string userType = "Belirtilmedi";
                switch (item.UserTypeText)
                {
                    case "Administrator": userType = "Administrator"; break;
                    case "Employee": userType = "Çalışan"; break;
                    case "Customer": userType = "Müşteri"; break;
                }
                selectListItems.Add(new SelectListItem() { Value = item.ID.ToString(), Text = userType });
            }
            return selectListItems;
        }
        #endregion

        #region Logs
        [AuthFilter(PageName = "logs")]
        public ActionResult LogPage(int ID = 0)
        {

            if (reloadViewBagFromSession("Log Kayıtları", EnumClass.Page.logs))
            {
                context = external.RefreshAll(context);
                var logList = context.Logs.OrderByDescending(x => x.CreatedTime).ToList();
                ViewBag.logList = logList;
                Log log = new Log();
                if (ID > 0)
                    log = context.Logs.FirstOrDefault(x => x.ID == ID);
                if (log == null)
                {
                    setProcess(EnumClass.ProcessType.warning, "Bu Kayıta Ulaşılamıyor.");
                    log = new Log();
                }
                return View(log);
            }
            else
            {
                return returnToLogin();
            }
        }

        #endregion

        #region Reports
        public ActionResult OpenIssuesReportPage()
        {
            if (reloadViewBagFromSession("Açık Talepler Rapor Sayfası", EnumClass.Page.openIssuesReport))
            {
                IssueReportViewModel issueReportViewModel = new IssueReportViewModel();
                issueReportViewModel.Customer = new User();
                issueReportViewModel.Employee = new User();
                List<Issue> issueList = getIssueList();
                int logged_user_id = ViewBag.logged_user.id;
                User loggedUser = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                if (loggedUser.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator))
                {
                    issueList = issueList.Where(x => !x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList();
                }
                else
                {
                    issueList = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()) || (x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString()) && x.OpenedBy.ID == logged_user_id)).ToList();
                }
                if (issueList.Count > 0)
                {
                    DateTime lowestDate = issueList.OrderBy(x => x.CreatedTime).First().CreatedTime;
                    issueReportViewModel.startTime = lowestDate.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                else
                {
                    issueReportViewModel.startTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                issueReportViewModel.endTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                return View(issueReportViewModel);
            }
            else
            {
                return returnToLogin();
            }
        }

        [HttpPost]
        public ActionResult OpenIssuesReportPage(IssueReportViewModel issueReportViewModel)
        {
            if (reloadViewBagFromSession("Açık Talepler Rapor Sayfası", EnumClass.Page.openIssuesReport))
            {
                List<Issue> issueList = getIssueList();
                int logged_user_id = ViewBag.logged_user.id;
                User loggedUser = context.Users.FirstOrDefault(x => x.ID == logged_user_id);
                if (loggedUser.UserType.UserTypeText.Equals(EnumClass.UserType.Administrator))
                {
                    issueList = issueList.Where(x => !x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList();
                }
                else
                {
                    issueList = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString()) || (x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString()) && x.OpenedBy.ID == logged_user_id)).ToList();
                }
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                if (issueReportViewModel != null)
                {
                    DateTime startTime = DateTime.Parse(issueReportViewModel.startTime),
                             endTime = DateTime.Parse(issueReportViewModel.endTime);
                    if (issueReportViewModel.Employee != null && issueReportViewModel.Employee.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Employee.ID) || x.OpenedBy.ID == issueReportViewModel.Employee.ID).ToList();
                    }
                    if (issueReportViewModel.Customer != null && issueReportViewModel.Customer.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Customer.ID) || x.OpenedBy.ID == issueReportViewModel.Customer.ID).ToList();
                    }
                    issueList = issueList.Where(x => DateTime.Compare(x.CreatedTime.Date, startTime.Date) >= 0 && DateTime.Compare(x.CreatedTime.Date, endTime.Date) <= 0).ToList();
                    ViewBag.issueList = issueList;
                    setProcess(EnumClass.ProcessType.success, "[ " + issueList.Count + " ] Adet Kayıt Filtrelendi");
                    return View(issueReportViewModel);
                }
                else
                {
                    setProcess(EnumClass.ProcessType.warning, "Bir Hata Meydana Geldi. Lütfen Tekrar Deneyiniz.");
                    return View(issueReportViewModel);
                }

            }
            else
            {
                return returnToLogin();
            }
        }

        public ActionResult CloseIssuesReportPage()
        {
            if (reloadViewBagFromSession("Kapalı Talepler Rapor Sayfası", EnumClass.Page.closeIssuesReport))
            {
                IssueReportViewModel issueReportViewModel = new IssueReportViewModel();
                issueReportViewModel.Customer = new User();
                issueReportViewModel.Employee = new User();
                List<Issue> issueList = getIssueList().Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList();
                if (issueList.Count > 0)
                {
                    DateTime lowestDate = issueList.OrderBy(x => x.CreatedTime).First().CreatedTime;
                    issueReportViewModel.startTime = lowestDate.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                else
                {
                    issueReportViewModel.startTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                issueReportViewModel.endTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                return View(issueReportViewModel);
            }
            else
            {
                return returnToLogin();
            }
        }

        [HttpPost]
        public ActionResult CloseIssuesReportPage(IssueReportViewModel issueReportViewModel)
        {
            if (reloadViewBagFromSession("Kapalı Talepler Rapor Sayfası", EnumClass.Page.closeIssuesReport))
            {
                List<Issue> issueList = getIssueList().Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList();
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                if (issueReportViewModel != null)
                {
                    DateTime startTime = DateTime.Parse(issueReportViewModel.startTime),
                             endTime = DateTime.Parse(issueReportViewModel.endTime);
                    if (issueReportViewModel.Employee != null && issueReportViewModel.Employee.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Employee.ID) || x.OpenedBy.ID == issueReportViewModel.Employee.ID).ToList();
                    }
                    if (issueReportViewModel.Customer != null && issueReportViewModel.Customer.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Customer.ID) || x.OpenedBy.ID == issueReportViewModel.Customer.ID).ToList();
                    }
                    issueList = issueList.Where(x => DateTime.Compare(x.CreatedTime.Date, startTime.Date) >= 0 && DateTime.Compare(x.CreatedTime.Date, endTime.Date) <= 0).ToList();
                    ViewBag.issueList = issueList;
                    setProcess(EnumClass.ProcessType.success, "[ " + issueList.Count + " ] Adet Kayıt Filtrelendi");
                    return View(issueReportViewModel);
                }
                else
                {
                    setProcess(EnumClass.ProcessType.warning, "Bir Hata Meydana Geldi. Lütfen Tekrar Deneyiniz.");
                    return View(issueReportViewModel);
                }

            }
            else
            {
                return returnToLogin();
            }
        }


        public ActionResult CompanyScoreReportPage(EnumClass.ProcessType @enum = EnumClass.ProcessType.initialize, string processMessage = "")
        {
            if (reloadViewBagFromSession("Firma Skor Rapor Sayfası", EnumClass.Page.companyScoreReport))
            {
                setProcess(@enum, processMessage);
                IssueReportViewModel issueReportViewModel = new IssueReportViewModel();
                issueReportViewModel.Customer = new User();
                issueReportViewModel.Employee = new User();
                List<Issue> issueList = getIssueList();
                if (issueList.Count > 0)
                {
                    DateTime lowestDate = issueList.OrderBy(x => x.CreatedTime).First().CreatedTime;
                    issueReportViewModel.startTime = lowestDate.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                else
                {
                    issueReportViewModel.startTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                issueReportViewModel.endTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");

                issueReportViewModel.issueCount = issueList.Count;
                issueReportViewModel.waitingIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())).ToList().Count();
                issueReportViewModel.openIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString())).ToList().Count();
                issueReportViewModel.closeIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList().Count();
                issueReportViewModel.criticalIssueCount = issueList.Where(x => x.Priority == 2).ToList().Count();
                issueReportViewModel.importantIssueCount = issueList.Where(x => x.Priority == 1).ToList().Count();
                issueReportViewModel.normalIssueCount = issueList.Where(x => x.Priority == 0).ToList().Count();

                TimeSpan totalDiff = new TimeSpan();
                foreach (Issue item in issueList)
                {
                    if (item.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString()) && item.ClosedTime != null && item.AppointedTime != null)
                    {
                        TimeSpan different = item.ClosedTime.Value - item.AppointedTime.Value;
                        totalDiff = totalDiff.Add(different);
                    }
                }

                issueReportViewModel.totalSupportTime = getTimeFromMilisecond(totalDiff.TotalMilliseconds);
                TempData["SearchData"] = issueReportViewModel;
                return View(issueReportViewModel);
            }
            else
            {
                return returnToLogin();
            }
        }

        [HttpPost]
        public ActionResult CompanyScoreReportPage(IssueReportViewModel issueReportViewModel)
        {
            if (reloadViewBagFromSession("Firma Skor Rapor Sayfası", EnumClass.Page.companyScoreReport))
            {
                List<Issue> issueList = getIssueList();
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                if (issueReportViewModel != null)
                {
                    DateTime startTime = DateTime.Parse(issueReportViewModel.startTime),
                             endTime = DateTime.Parse(issueReportViewModel.endTime);
                    if (issueReportViewModel.Employee != null && issueReportViewModel.Employee.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Employee.ID) || x.OpenedBy.ID == issueReportViewModel.Employee.ID).ToList();
                    }
                    if (issueReportViewModel.Customer != null && issueReportViewModel.Customer.ID > 0)
                    {
                        issueList = issueList.Where(x => x.OpenedBy.ID == issueReportViewModel.Customer.ID).ToList();
                    }
                    issueList = issueList.Where(x => DateTime.Compare(x.CreatedTime.Date, startTime.Date) >= 0 && DateTime.Compare(x.CreatedTime.Date, endTime.Date) <= 0).ToList();
                    ViewBag.issueList = issueList;

                    issueReportViewModel.issueCount = issueList.Count;
                    issueReportViewModel.waitingIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())).ToList().Count();
                    issueReportViewModel.openIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString())).ToList().Count();
                    issueReportViewModel.closeIssueCount = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList().Count();
                    issueReportViewModel.criticalIssueCount = issueList.Where(x => x.Priority == 2).ToList().Count();
                    issueReportViewModel.importantIssueCount = issueList.Where(x => x.Priority == 1).ToList().Count();
                    issueReportViewModel.normalIssueCount = issueList.Where(x => x.Priority == 0).ToList().Count();

                    TimeSpan totalDiff = new TimeSpan();
                    foreach (Issue item in issueList)
                    {
                        if (item.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString()) && item.ClosedTime != null && item.AppointedTime != null)
                        {
                            TimeSpan different = item.ClosedTime.Value - item.AppointedTime.Value;
                            totalDiff = totalDiff.Add(different);
                        }
                    }

                    issueReportViewModel.totalSupportTime = getTimeFromMilisecond(totalDiff.TotalMilliseconds);
                    TempData["SearchData"] = issueReportViewModel;
                    setProcess(EnumClass.ProcessType.success, "[ " + issueList.Count + " ] Adet Kayıt Filtrelendi");
                    return View(issueReportViewModel);
                }
                else
                {
                    TempData["SearchData"] = issueReportViewModel;
                    setProcess(EnumClass.ProcessType.warning, "Bir Hata Meydana Geldi. Lütfen Tekrar Deneyiniz.");
                    return View(issueReportViewModel);
                }

            }
            else
            {
                return returnToLogin();
            }
        }

        public ActionResult FilterCompanyScoreReportPage(string data)
        {
            if (reloadViewBagFromSession("Firma Skor Rapor Sayfası", EnumClass.Page.companyScoreReport))
            {
                IssueReportViewModel issueReportViewModel = TempData["SearchData"] as IssueReportViewModel;

                List<Issue> issueList = getIssueList();
                ViewBag.issueList = issueList;
                ViewBag.employeeList = getEmployeeListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                if (issueReportViewModel != null)
                {
                    DateTime startTime = DateTime.Parse(issueReportViewModel.startTime),
                             endTime = DateTime.Parse(issueReportViewModel.endTime);
                    if (issueReportViewModel.Employee != null && issueReportViewModel.Employee.ID > 0)
                    {
                        issueList = issueList.Where(x => (x.AppointedUser != null && x.AppointedUser.ID == issueReportViewModel.Employee.ID) || x.OpenedBy.ID == issueReportViewModel.Employee.ID).ToList();
                    }
                    if (issueReportViewModel.Customer != null && issueReportViewModel.Customer.ID > 0)
                    {
                        issueList = issueList.Where(x => x.OpenedBy.ID == issueReportViewModel.Customer.ID).ToList();
                    }
                    issueList = issueList.Where(x => DateTime.Compare(x.CreatedTime.Date, startTime.Date) >= 0 && DateTime.Compare(x.CreatedTime.Date, endTime.Date) <= 0).ToList();
                   
                    TempData["SearchData"] = issueReportViewModel;
                    switch (data)
                    {
                       case "waiting":
                            issueList = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Beklemede.ToString())).ToList();
                            ViewBag.Filter = "Bekleyen Talepler";
                            break;
                        case "open":
                            issueList = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Aktif.ToString())).ToList();
                            ViewBag.Filter = "Aktif Talepler";
                            break;
                        case "done":
                            issueList = issueList.Where(x => x.Status.StatusType.Equals(EnumClass.StatusType.Tamamlandı.ToString())).ToList();
                            ViewBag.Filter = "Tamamlanan Talepler";
                            break;
                        case "critical":
                            issueList = issueList.Where(x => x.Priority == 2).ToList();
                            ViewBag.Filter = "Kritik Talepler";
                            break;
                        case "important":
                            issueList = issueList.Where(x => x.Priority == 1).ToList();
                            ViewBag.Filter = "Önemli Talepler";
                            break;
                        case "normal":
                            issueList = issueList.Where(x => x.Priority == 0).ToList();
                            ViewBag.Filter = "Normal Talepler";
                            break;
                        default:
                            ViewBag.Filter = "Tümü";
                            break;
                    }
                    ViewBag.issueList = issueList;
                    return View("CompanyScoreReportPage", issueReportViewModel);
                }
                else
                {
                    return RedirectToAction("CompanyScoreReportPage", new { @enum = EnumClass.ProcessType.warning, processMessage = "Bir Hata Meydana Geldi. Lütfen Tekrar Deneyiniz." });
                }

            }
            else
            {
                return returnToLogin();
            }
        }

        public ActionResult EmployeeReportPage()
        {
            if (reloadViewBagFromSession("Personel Rapor Sayfası", EnumClass.Page.employeeReport))
            {
                IssueReportViewModel issueReportViewModel = new IssueReportViewModel();
                issueReportViewModel.Customer = new User();
                issueReportViewModel.Status = new Status();
                List<Issue> issueList = getIssueList();
                if (issueList.Count > 0)
                {
                    DateTime lowestDate = issueList.OrderBy(x => x.CreatedTime).First().CreatedTime;
                    issueReportViewModel.startTime = lowestDate.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                else
                {
                    issueReportViewModel.startTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                }
                issueReportViewModel.endTime = DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/");
                ViewBag.issueList = issueList;
                ViewBag.statusList = getStatusListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                return View(issueReportViewModel);
            }
            else
            {
                return returnToLogin();
            }
        }

        [HttpPost]
        public ActionResult EmployeeReportPage(IssueReportViewModel issueReportViewModel)
        {
            if (reloadViewBagFromSession("Personel Rapor Sayfası", EnumClass.Page.employeeReport))
            {
                List<Issue> issueList = getIssueList();
                ViewBag.issueList = issueList;
                ViewBag.statusList = getStatusListForDropDown("Hepsi");
                ViewBag.customerList = getCustomerListForDropDown("Hepsi");
                if (issueReportViewModel != null)
                {
                    DateTime startTime = DateTime.Parse(issueReportViewModel.startTime),
                             endTime = DateTime.Parse(issueReportViewModel.endTime);
                    if (issueReportViewModel.Customer != null && issueReportViewModel.Customer.ID > 0)
                    {
                        issueList = issueList.Where(x => x.OpenedBy != null && x.OpenedBy.ID == issueReportViewModel.Customer.ID).ToList();
                    }
                    if (issueReportViewModel.Status != null && issueReportViewModel.Status.ID > 0)
                    {
                        issueList = issueList.Where(x => x.Status != null && x.Status.ID == issueReportViewModel.Status.ID).ToList();
                    }
                    issueList = issueList.Where(x => DateTime.Compare(x.CreatedTime.Date, startTime.Date) >= 0 && DateTime.Compare(x.CreatedTime.Date, endTime.Date) <= 0).ToList();
                    ViewBag.issueList = issueList;
                    setProcess(EnumClass.ProcessType.success, "[ " + issueList.Count + " ] Adet Kayıt Filtrelendi");
                    return View(issueReportViewModel);
                }
                else
                {
                    setProcess(EnumClass.ProcessType.warning, "Bir Hata Meydana Geldi. Lütfen Tekrar Deneyiniz.");
                    return View(issueReportViewModel);
                }

            }
            else
            {
                return returnToLogin();
            }
        }

        public JsonResult GetIssues()
        {
            try
            {
                using (external.RefreshAll(context))
                {
                    var myList = context.Issues.ToList();
                    return Json(myList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region External Methods

        public string getTimeFromMilisecond(double milisec)
        {
            double msec = milisec;
            int mins = (int)Math.Floor((decimal)(msec / 60000));
            int hrs = (int)Math.Floor((decimal)(mins / 60));
            int days = (int)Math.Floor((decimal)(hrs / 24));
            int yrs = (int)Math.Floor((decimal)(days / 365));
            int secs = (int)Math.Floor((decimal)((msec / 100) % 60));
            mins = mins % 60;
            hrs = hrs % 24;
            var dateDiff = "-";
            if (secs > 0 || mins > 0 || hrs > 0 || days > 0)
            {
                if (days > 0)
                {
                    dateDiff = days + " Gün";
                }
                if (days > 0 && hrs > 0)
                {
                    dateDiff += ", " + hrs + " Saat";
                }
                else if (hrs > 0)
                {
                    dateDiff = hrs + " Saat";
                }
                if ((days > 0 || hrs > 0) && mins > 0)
                {
                    dateDiff += ", " + mins + " Dakika";
                }
                else if (mins > 0)
                {
                    dateDiff = mins + " Dakika";
                }
                if ((days > 0 || hrs > 0 || mins > 0) && secs > 0)
                {
                    dateDiff += ", " + secs + " Saniye";
                }
                else if (secs > 0)
                {
                    dateDiff = secs + " Saniye";
                }
            }
            return dateDiff;
        }

        public bool reloadViewBagFromSession(string pageTitle, EnumClass.Page pageName)
        {
            bool returnValue = false;
            try
            {
                if (Session["logged_user"] != null && Session["deniedPages"] != null)
                {
                    ViewBag.Title = pageTitle;
                    ViewBag.currentPage = pageName.ToString().ToLowerInvariant();
                    ViewBag.logged_user = Session["logged_user"];
                    ViewBag.userType = ViewBag.logged_user.userType;
                    ViewBag.deniedPages = Session["deniedPages"];
                    ViewBag.res1 = Resources.zDeveloper;
                    ViewBag.res2 = Resources.zDeveloper2;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                setProcess(EnumClass.ProcessType.danger);
                external.addLog(EnumClass.LogType.error, ex.ToString(), "HomeController", "reloadViewBagFromSession");
            }
            return returnValue;
        }

        public ActionResult returnToLogin(EnumClass.ProcessType enumValue = EnumClass.ProcessType.warning, string message = "Sayfa zaman aşımına uğradı")
        {
            return RedirectToRoute("Login", new { @enum = enumValue, processMessage = message });
        }

        public void setProcess(EnumClass.ProcessType processType, string processMessage = null, bool waitParameter = false)
        {
            try
            {
                if (processType != EnumClass.ProcessType.initialize)
                {
                    if (processType == EnumClass.ProcessType.danger && String.IsNullOrEmpty(processMessage))
                    {
                        processMessage = "Bir hata meydana geldi lütfen yetkiliye bildirin.";
                    }
                    ViewBag.processType = processType.ToString();
                    ViewBag.processMessage = processMessage;
                    if (waitParameter)
                    {
                        ViewBag.processWait = 3000;
                    }
                }
            }
            catch (Exception ex)
            {
                external.addLog(EnumClass.LogType.error, ex.ToString(), "HomeController", "setProcess");
            }
        }

        [HttpPost]
        public void SetTempData(string key, string value)
        {
            // Set your TempData key to the value passed in
            TempData[key] = value;
        }

        #endregion
    }
}