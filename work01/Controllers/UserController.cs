﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using work01.Models.Entities;
using work01.Models.Managers;
using SendGrid.Helpers.Mail;
using work01.App_Start;
using work01.Models.Enums;
using work01.Content;

namespace work01.Controllers
{
    [ExceptionHandler]
    public class UserController : Controller
    {
        DatabaseContext context;
        External external = new External();

        #region LoginPage
        public ActionResult LoginPage(Models.Enums.EnumClass.ProcessType @enum = Models.Enums.EnumClass.ProcessType.initialize, string processMessage = "", bool logout=false)
        {
            if (logout) {
                @enum = Models.Enums.EnumClass.ProcessType.success;
                processMessage = "Oturum Kapatıldı.";
            }
            setProcess(@enum, processMessage);
            clearSession();
            reloadViewBagFromSession();
            return View(createEmptyUser());
        }
        [HttpPost]
        public ActionResult LoginPage(User user)
        {
            reloadViewBagFromSession();
            context = new DatabaseContext();
            string md5Pass = external.GenerateSha256(user.Password);
            User confirmedUser = context.Users.FirstOrDefault(x => x.Username.Equals(user.Username) && x.Password.Equals(md5Pass));
            if (confirmedUser != null)
            {
                List<Permission> permissionLimits = context.Permissions.Where(x => x.UserType.ID == confirmedUser.UserType.ID && !x.pass).ToList();
                List<WebPageClass> pageList = permissionLimits.Select(x => x.Page).ToList();
                var logged_user = new { id = confirmedUser.ID, username = confirmedUser.Username, userNameSurname = confirmedUser.Person.Name + " " + confirmedUser.Person.SurName, userTypeId = confirmedUser.UserType.ID, userType = confirmedUser.UserType.UserTypeText };
                var deniedPages = pageList;
                //new Sendgrid().SendEMail(new EmailAddress("kadir.kalkan3475@gmail.com", "Kadir"), Models.Enums.Enum.MailType.welcome);
                //new Sendgrid().SendEMail(new EmailAddress("kadir.kalkan3475@gmail.com", "Kadir"), Models.Enums.Enum.MailType.forgotPass, btnURL: "http://localhost:57295/User/RePasswordPage");
                //List<string> mailContent = new List<string>() { "kadir", "kalkan", "30 günlük deneme", "süresinin sonuna geldi" };
                //new Sendgrid().SendEMail(new EmailAddress("kadir.kalkan3475@gmail.com", "Kadir"), Models.Enums.Enum.MailType.reminder, mailContent: mailContent);
                Session["logged_user"] = logged_user;
                Session["deniedPages"] = deniedPages;
                return RedirectToRoute("Index", new { @enum = Models.Enums.EnumClass.ProcessType.success, processMessage = "Başarıyla Giriş Yaptınız." });
            }
            else
            {
                setProcess(Models.Enums.EnumClass.ProcessType.danger, "Kullanıcı adı veya Şifrenizi Hatalı Girdiniz");
                User newUser = createEmptyUser(user.Username, user.Password);
                return View(newUser);
            }
        }
        private User createEmptyUser(string username = "", string password = "")
        {
            return new User()
            {
                Username = username,
                Password = password,
                PasswordAgain = password,
                Email = "xyz@unknown.com",
                Person = new Person(),
                UserType = new UserType()
            };
        }
        #endregion

        #region ForgotPasswordPage
        public ActionResult ForgotPasswordPage()
        {
            try
            {
                clearSession();
                reloadViewBagFromSession();
                User user = new User();
                return View(user);
            }
            catch (Exception ex)
            {
                setProcess(Models.Enums.EnumClass.ProcessType.danger);
                external.addLog(Models.Enums.EnumClass.LogType.error, ex.ToString(), "UserController", "ForgotPasswordPage");
                return RedirectToRoute("Login", new { @enum = Models.Enums.EnumClass.ProcessType.danger });
            }
        }

        [HttpPost]
        public ActionResult ForgotPasswordPage(User user)
        {
            try
            {
                reloadViewBagFromSession();
                context = new DatabaseContext();
                if (user.Email != null)
                {
                    User confirmedUser = context.Users.FirstOrDefault(x => x.Email == user.Email);
                    if (confirmedUser != null)
                    {
                        confirmedUser.GUID = Guid.NewGuid();
                        new Sendgrid().SendEMail(new EmailAddress(confirmedUser.Email, confirmedUser.Person.Name + " " + confirmedUser.Person.SurName), Models.Enums.EnumClass.MailType.forgotPass, btnURL: external.getPageURL(Models.Enums.EnumClass.Page.repass) + "?GUID=" + confirmedUser.GUID, text: confirmedUser.Username);
                        setProcess(Models.Enums.EnumClass.ProcessType.success, "Şifre yenileme bağlantısı gönderildi. Lütfen mailinizi kontrol ediniz.");
                        context.SaveChanges();
                        return View();
                    }
                    else
                    {
                        setProcess(Models.Enums.EnumClass.ProcessType.danger, "Bu bilgilere ait kullanıcı bulunamadı.");
                        return View(user);
                    }
                }
                else
                {
                    setProcess(Models.Enums.EnumClass.ProcessType.danger, "Boş alanları doldurun.");
                    return View(user);
                }
            }
            catch (Exception ex)
            {
                setProcess(Models.Enums.EnumClass.ProcessType.danger);
                external.addLog(Models.Enums.EnumClass.LogType.error, ex.ToString(), "UserController", "ForgotPasswordPagePost");
                return View(user);
            }

        }
        #endregion

        #region RePasswordPage
        public ActionResult RePasswordPage(string GUID)
        {
            clearSession();
            reloadViewBagFromSession();
            context = new DatabaseContext();
            Guid userGuid = Guid.NewGuid();
            if (GUID != null)
            {
                userGuid = Guid.Parse(GUID);
            }
            User confirmedUser = context.Users.FirstOrDefault(x => x.GUID.Equals(userGuid));
            if (confirmedUser == null)
            {
                return RedirectToRoute("Login", new { @enum = Models.Enums.EnumClass.ProcessType.danger, processMessage = "Bağlantı Zaman Aşımına Uğradı" });
            }
            else
            {
                TempData["rePassUser"] = confirmedUser;
                return View(new User());
            }
        }

        [HttpPost]
        public ActionResult RePasswordPage(User user)
        {
            User tempUser = (User)TempData["rePassUser"];
            context = new DatabaseContext();
            reloadViewBagFromSession();
            if (user.Password == user.PasswordAgain)
            {
                User confirmedUser = context.Users.FirstOrDefault(x => x.ID == tempUser.ID);
                confirmedUser.Password = external.GenerateSha256(user.Password);
                confirmedUser.GUID = Guid.NewGuid();
                context.SaveChanges();
                return RedirectToRoute("Login", new { @enum = Models.Enums.EnumClass.ProcessType.success, processMessage = "Şifre Değiştirme İşlemi Başarılı" });
            }
            else
            {
                TempData["rePassUser"] = tempUser;
                setProcess(Models.Enums.EnumClass.ProcessType.warning, "Lütfen Şifreleri Aynı Girin.");
                return View(user);
            }
        }
        #endregion

        #region external Methods
        public void setProcess(Models.Enums.EnumClass.ProcessType processType, string processMessage = null, bool waitParameter = false)
        {
            try
            {
                reloadViewBagFromSession();
                if (processType != Models.Enums.EnumClass.ProcessType.initialize)
                {
                    if (processType == Models.Enums.EnumClass.ProcessType.danger && String.IsNullOrEmpty(processMessage))
                    {
                        processMessage = "Bir hata meydana geldi lütfen yetkiliye bildirin.";
                    }
                    ViewBag.processType = processType.ToString();
                    ViewBag.processMessage = processMessage;
                    if (waitParameter)
                    {
                        ViewBag.processWait = 3000;
                    }
                }
            }
            catch (Exception ex)
            {
                external.addLog(Models.Enums.EnumClass.LogType.error, ex.ToString(), "UserController", "setProcess");
            }
        }
        public bool reloadViewBagFromSession()
        {
            bool returnValue = false;
            try
            {
                    ViewBag.res1 = Resources.zDeveloper;
                    ViewBag.res2 = Resources.zDeveloper2;
                    returnValue = true;
            }
            catch (Exception ex)
            {
                setProcess(EnumClass.ProcessType.danger);
                external.addLog(EnumClass.LogType.error, ex.ToString(), "UserController", "reloadViewBagFromSession");
            }
            return returnValue;
        }

        public void clearSession()
        {
            Session.Clear();
        }
        #endregion
    }
}