﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using work01.Models.Entities;
using work01.Models.Managers;

namespace work01.Filters
{
    public class AuthFilter : FilterAttribute, IAuthorizationFilter
    {
        public string PageName { get; set; }
        External external = new External();
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.Session["logged_user"] == null || filterContext.HttpContext.Session["deniedPages"] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "LoginPage", controller = "User", @enum = Models.Enums.EnumClass.ProcessType.warning, processMessage = "Sistem Zaman Aşımına Uğradı." }));
                }
                else
                {

                    var filterAttribute = filterContext.ActionDescriptor.GetFilterAttributes(true)
                                      .Where(a => a.GetType() ==
                                     typeof(AuthFilter));
                    if (filterAttribute != null)
                    {
                        if (!external.IsAccessible(PageName, filterContext.HttpContext.Session["deniedPages"] as List<WebPageClass>))
                            filterContext.Result = new RedirectToRouteResult(
                                new RouteValueDictionary(
                                    new
                                    {
                                        action = "Index",
                                        controller = "Home",
                                        @enum = Models.Enums.EnumClass.ProcessType.warning,
                                        processMessage = "Bu Sayfaya Giriş İzniniz Bulunmamaktadır."
                                    }));
                        //filterContext.Result = new HttpUnauthorizedResult();
                    }

                }
            }
            catch (Exception ex) {
                external.addLog(Models.Enums.EnumClass.LogType.error, ex.ToString(),"AuthFilter", "OnAuthorization");
            }
        }
    }
}